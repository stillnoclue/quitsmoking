-- MySQL dump 10.13  Distrib 5.7.15, for Win64 (x86_64)
--
-- Host: localhost    Database: group5
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `board`
--

DROP TABLE IF EXISTS `board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `board` (
  `board_id` int(11) NOT NULL AUTO_INCREMENT,
  `board_name` varchar(255) DEFAULT NULL,
  `last_post_date` varchar(255) DEFAULT '1970-01-01 00:00:00.000',
  `last_post_id` int(11) DEFAULT '0',
  `post_count` int(11) DEFAULT '0',
  PRIMARY KEY (`board_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board`
--

LOCK TABLES `board` WRITE;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
INSERT INTO `board` VALUES (1,'Member Communication','2016-10-04 20:11:23.097',0,0),(2,'GP Q&A','2016-10-04 20:11:23.097',0,0),(3,'Share Your Story','2016-10-04 20:11:23.097',0,0);
/*!40000 ALTER TABLE `board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(800) DEFAULT NULL,
  `commented_date` datetime DEFAULT NULL,
  `Post_PID` int(11) NOT NULL,
  `User_UID` int(11) NOT NULL,
  PRIMARY KEY (`CID`),
  UNIQUE KEY `CID_UNIQUE` (`CID`),
  KEY `fk_Comment_Post1_idx` (`Post_PID`),
  KEY `fk_Comment_User1_idx` (`User_UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gp_patient`
--

DROP TABLE IF EXISTS `gp_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gp_patient` (
  `assigned_date` datetime DEFAULT NULL,
  `GPID` int(11) NOT NULL,
  `Patient_ID` int(11) NOT NULL,
  PRIMARY KEY (`GPID`,`Patient_ID`),
  KEY `fk_GP_Patient_User2_idx` (`Patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gp_patient`
--

LOCK TABLES `gp_patient` WRITE;
/*!40000 ALTER TABLE `gp_patient` DISABLE KEYS */;
/*!40000 ALTER TABLE `gp_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `if_read` tinyint(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `reply_id` int(11) DEFAULT NULL,
  `reply_message_id` varchar(45) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `content` text,
  `post_id` int(20) NOT NULL AUTO_INCREMENT,
  `time` varchar(255) DEFAULT NULL,
  `topic` varchar(255) DEFAULT NULL,
  `author` bigint(20) NOT NULL,
  `board_id` int(11) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES ('I have been trying to quit for the past two weeks and have only managed to go from half a pack a day to two cigarettes a day. I hate being so dependent on cigarettes but struggle constantly with feeling like i need to keep that dark side of my self. I hate even writing this because it sounds so.. stupid and needy and just sad. Like grow up, self, youre not a \"dark and mysterious\" 16 year old theatre kid anymore. Has anyone else struggled with this feeling? That cigarettes are a part of your identity? Its almost midnight and i will start again tomorrow and try harder but if anyone has any motivation or advice, please throw it my way.\r\n',1,'2016-10-04 20:11:23.097','have failed day after day and need advice',11,00000000001),('I have been trying to quit for the past two weeks and have only managed to go from half a pack a day to two cigarettes a day. I hate being so dependent on cigarettes but struggle constantly with feeling like i need to keep that dark side of my self. I hate even writing this because it sounds so.. stupid and needy and just sad. Like grow up, self, youre not a \"dark and mysterious\" 16 year old theatre kid anymore. Has anyone else struggled with this feeling? That cigarettes are a part of your identity? Its almost midnight and i will start again tomorrow and try harder but if anyone has any motivation or advice, please throw it my way.',2,'2016-10-04 20:11:23.097','have failed day after day and need advice',15,00000000002),('I wouldn\'t have really thought about the date except for an instance of persistent irritation I\'ve had to deal with for the last week or so and it dawned on me that that irritation was comparable to the addiction of cigarettes, but on a much subtler note. It made me smile understanding that this irritation has given me less stress than my average past days of smoking.\r\nIt still feels like last month that I quit... well except that I\'m profoundly more active, and healthier - I\'ve been running 3 days a week since I quit, off and on. Happy with my legs, I\'ve now started cross-training for an improved upper body. \r\nI have absolutely no desires for cigarettes and would probably choke on the f thing if I tried. I hate smelling the things, but I live with smokers so it is a constant thing I have to deal with. This arrangement has effected my dreams on more than one occasion. But alas, I\'m unable to change the people around me. I can only change myself.',3,'2016-10-04 20:11:23.097','1101 days or about 3 years',11,00000000003),('I wouldn\'t have really thought about the date except for an instance of persistent irritation I\'ve had to deal with for the last week or so and it dawned on me that that irritation was comparable to the addiction of cigarettes, but on a much subtler note. It made me smile understanding that this irritation has given me less stress than my average past days of smoking.\r\nIt still feels like last month that I quit... well except that I\'m profoundly more active, and healthier - I\'ve been running 3 days a week since I quit, off and on. Happy with my legs, I\'ve now started cross-training for an improved upper body. \r\nI have absolutely no desires for cigarettes and would probably choke on the f thing if I tried. I hate smelling the things, but I live with smokers so it is a constant thing I have to deal with. This arrangement has effected my dreams on more than one occasion. But alas, I\'m unable to change the people around me. I can only change myself.\r\n',15,'2016-10-26T04:19:26.591','1101 days or about 3 years',11,00000000001),('I had my last cigarette December 31, 2015. I used Champix and read the Easy Way. I haven\'t had even a drag or any nicotine all year, but I still want to smoke so bad. I crave it multiple times a day, certainly less frequently and powerfully than in the beginning. I walk down the street and I envy smokers, I definitely still romanticize it, and I love the smell of smoke. I\'m not sure if I got older (27 now) and I became more habituated to smoking (I smoked from when I was 16).\r\nI had one previous quit attempt where I went over a year when I was 21, and the desire to smoke was gone completely in months. I thought cigarettes smelled gross and tried to avoid being near smokers. It baffles me I still want to smoke this time.\r\nI\'d love any insight from anyone with a similar experience. I really don\'t want to start smoking again, but I\'m afraid I might cave to temptation one day.\r\n',16,'2016-10-26T04:20:00.681','Still really want to smoke almost ten months in....',11,00000000001),('I wouldn\'t have really thought about the date except for an instance of persistent irritation I\'ve had to deal with for the last week or so and it dawned on me that that irritation was comparable to the addiction of cigarettes, but on a much subtler note. It made me smile understanding that this irritation has given me less stress than my average past days of smoking.\r\nIt still feels like last month that I quit... well except that I\'m profoundly more active, and healthier - I\'ve been running 3 days a week since I quit, off and on. Happy with my legs, I\'ve now started cross-training for an improved upper body. \r\nI have absolutely no desires for cigarettes and would probably choke on the f thing if I tried. I hate smelling the things, but I live with smokers so it is a constant thing I have to deal with. This arrangement has effected my dreams on more than one occasion. But alas, I\'m unable to change the people around me. I can only change myself.\r\n',17,'2016-10-26T04:21:17.600','1101 days or about 3 years',16,00000000002),('I had my last cigarette December 31, 2015. I used Champix and read the Easy Way. I haven\'t had even a drag or any nicotine all year, but I still want to smoke so bad. I crave it multiple times a day, certainly less frequently and powerfully than in the beginning. I walk down the street and I envy smokers, I definitely still romanticize it, and I love the smell of smoke. I\'m not sure if I got older (27 now) and I became more habituated to smoking (I smoked from when I was 16).\r\nI had one previous quit attempt where I went over a year when I was 21, and the desire to smoke was gone completely in months. I thought cigarettes smelled gross and tried to avoid being near smokers. It baffles me I still want to smoke this time.\r\nI\'d love any insight from anyone with a similar experience. I really don\'t want to start smoking again, but I\'m afraid I might cave to temptation one day.\r\n',18,'2016-10-26T04:21:43.649','Still really want to smoke almost ten months in....',16,00000000003),('I actually sleeping a lot of hours since I cut down the smoke. Today was my first 24 hours and sadly i did not have any work done. I think i will have to run a treadmill and change the atmosphere around me to get into better mood. See you guys later and good luck to you all',19,'2016-10-26T04:22:45.019','1 day from last smoke',16,00000000003),('I\'ve been trying to quit for the last few months and I find it very difficult to keep up with school work and keep up with interests like music and art whenever I\'m not smoking. Whenever I\'ve quit, all I\'ve been able to do is durdle around.\r\nI really do want to stop, but I have so much trouble trying to focus on studying and such. I\'m feeling pretty trapped, not being able to function properly without nicotine. Unfortunately, I can never find a good time to stop, exams for my classes are spaced fairly close together this semester. Does anyone have advice for quitting and still maintaining my normal motivation and energy?',20,'2016-10-26T07:58:19.609','Having trouble maintaining my normal motivation while quitting',20,00000000002),('So it\'s like I go to community college and there\'s nothing to do between classes so I keep going out to the smoking spot which I like because there\'s people there I talk to and smoking is what goes on there so I just started buying packs. But I feel guilty about it because it\'s costly, my parents don\'t know and I don\'t like keeping anything from them, and it\'s bad for me and the actual act of smoking doesn\'t make me feel good. Also I\'ve been going to school like two hours specifically to stand around talk to people and smoke . I need help quitting but I don\'t know where to go instead of the smoke shack and I really don\'t want to have this conversion with my parents. Advice?',21,'2016-10-26T07:58:45.534','I\'m thinking about quitting because I can\'t figure out why I\'m even smoking',20,00000000002),('I\'m in college, and a lot of my interaction comes from sitting around the smoking area, chainsmoking and talking to people. I have no idea what I would even do if I would quit. I have no motivation, and I don\'t really have the discipline to quit. I\'m really just looking for any sort of advice, on how to quit!\r\n',22,'2016-10-26T08:02:29.211','How should I go about quitting smoking?',20,00000000001),('I\'ve been trying to quit for the last few months and I find it very difficult to keep up with school work and keep up with interests like music and art whenever I\'m not smoking. Whenever I\'ve quit, all I\'ve been able to do is durdle around.\r\nI really do want to stop, but I have so much trouble trying to focus on studying and such. I\'m feeling pretty trapped, not being able to function properly without nicotine. Unfortunately, I can never find a good time to stop, exams for my classes are spaced fairly close together this semester. Does anyone have advice for quitting and still maintaining my normal motivation and energy?',23,'2016-10-26T08:04:48.187','Having trouble maintaining my normal motivation while quitting',15,00000000001);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reply` (
  `reply_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `content` text,
  `post_id` int(11) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
INSERT INTO `reply` VALUES (1,16,'Hello world!',4,'2016-10-25T16:45:17.158'),(2,16,'I smoked for 20 years, quit for 5, started back, quit, smoked 9 more years, then quit again. It is worse than heroine, and it takes a few attempts, but dont give up. Take it from a chronic smoker, it can be done. I\'m 46 and haven\'t smoked for 7 years. I\'ll never smoke again. I know what a terrible habit it is now. How did I do it? I took an ordinary household drinking straw, cut it in half, and kept it on my person at all times. When the urge came, I puffed the straw. When you feel the urge, smoke the straw. It sounds silly, but I swear, it works. When the urge gets very strong, get busy with something else that makes it impossible to smoke. \r\nAlso you must be spiritually strong, and stay away from other smokers. Pretty soon you\'ll look back and will not be able to imagine yourself ever smoking. Really. You\'ll be a nonsmoker and you\'ll see that it isn\'t the person that you are anymore. Do I ever get the urge? Once in awhile, but not smoking is much better than smoking and I know this now. It is cleaner, and people have a different view of you. They see you as a cleaner person. More wholesome. Spiritually strong. Good luck!!',5,'2016-10-25T16:53:09.801'),(3,16,'I am 50 years old and I smoked for 30 years. I quit smoking 7 months ago using the nicotine gum. It was one of the hardest things I have ever done. I have heard some people say that the worse s over in about 2 weeks. I wasn\'t that fortunate, it took about 3 months , but now , I seldom think about a cigarette The gum helped with the physical addiction to the nicotine, but the big step is you just don\'t buy them. Believe me, it will take some time but you will get to the point that you can stop off at the store and not even think about buying a pack.',5,'2016-10-25T16:53:45.561'),(4,16,'\r\nNatural Quit Smoking Magic\r\nSource(s):\r\nhttps://tinyurl.im/aHKbK',6,'2016-10-25T16:54:57.308'),(5,16,'On a song from no smoking! \r\n\r\n\r\nReally',6,'2016-10-25T17:14:21.374'),(6,11,'good',1,'2016-10-26T01:08:20.630'),(7,11,'dfsfsdfdvvcc',2,'2016-10-26T02:23:45.043'),(8,11,'vfdbfd',1,'2016-10-26T02:34:46.578'),(9,12,'Think about it this way:\r\nYou didn\'t pick up the habit of smoking from your first cigarette, or even your tenth. It happened over time, yeah?\r\nSo, think about quitting as doing the same in reverse. Smoking a little bit less, and less. Each time, slimming your intake by an amount that YOU think is reasonable.\r\nHour by hour. Day by day. And weeks soon after.\r\nInstead of calling it \"quitting\", call it \"slowing the habit\".\r\nThis way, you\'ll be happier with the measurable change of smoking a few less cigarettes a day, instead of hating yourself for not doing better.\r\nThere\'s a reason why being able to quit cold turkey is considered a virtue. It\'s important not to rush yourself and lose patience.\r\n',1,'2016-10-26T04:17:46.986'),(10,12,'Think about it this way:\r\nYou didn\'t pick up the habit of smoking from your first cigarette, or even your tenth. It happened over time, yeah?\r\nSo, think about quitting as doing the same in reverse. Smoking a little bit less, and less. Each time, slimming your intake by an amount that YOU think is reasonable.\r\nHour by hour. Day by day. And weeks soon after.\r\nInstead of calling it \"quitting\", call it \"slowing the habit\".\r\nThis way, you\'ll be happier with the measurable change of smoking a few less cigarettes a day, instead of hating yourself for not doing better.\r\nThere\'s a reason why being able to quit cold turkey is considered a virtue. It\'s important not to rush yourself and lose patience.',2,'2016-10-26T04:18:35.762'),(11,20,'Relearn how you think about cigarettes and others that smoke them.\r\nMany people give in because, in their head, they are being \"denied\" a cigarette or enjoyment. \"why does that asshole get to smoke while I sit here suffering\" Instead, you should recognize that you don\'t actually want them or you wouldn\'t be trying to quit. In fact, you should probably feel sorry for those people since they are still stuck in that addiction and aren\'t trying at all.\r\nYour mind plays many other tricks in order to excuse itself to start smoking again. One of my best tricks was to make a personalized bracelet that serves as a reminder of my commitment.I wore that thing for almost a year. ( made it too snug) It works so well that you wont even be able to think about cigarettes without taking note of it. More importantly, If you chose to DO pick up a cigarette. That bracelet reminds you that if you do, its your own damn fault. NO EXCUSES.\r\nWhere I learned these tricks among others was from a book called Carr\'s easyway to stop smoking. The bracelet thing I\'m not sure if that was explicitly from that book, but learned a basic idea of it 20 years before I quit lol.\r\n',17,'2016-10-26T08:00:24.349'),(12,20,'Hey, I think I understand and have been through a similar thing. I\'ve made it to three months several times and then I tend to cave. I\'m trying to work out how I can overcome that this time. I have the same thing about romanticising smoking and I like the smell of smoke too. I suppose it comes down to reminding yourself of the negatives and weighing up whether you think it\'s worth it or not. Throughout my twenties it seemed to work for me. However, I\'m almost thirty now and I notice the effects more such as a chronic cough and shorter breath. I wish I could just have a cig now and then but it just doesn\'t work like that.\r\n',18,'2016-10-26T08:01:14.445'),(13,20,'How did you quit while living with smokers. I do and i find it so hard not to just walk a couple steps and take a cig from one of them.',3,'2016-10-26T08:01:56.740'),(14,20,'Relearn how you think about cigarettes and others that smoke them.\r\nMany people give in because, in their head, they are being \"denied\" a cigarette or enjoyment. \"why does that asshole get to smoke while I sit here suffering\" Instead, you should recognize that you don\'t actually want them or you wouldn\'t be trying to quit. In fact, you should probably feel sorry for those people since they are still stuck in that addiction and aren\'t trying at all.\r\nYour mind plays many other tricks in order to excuse itself to start smoking again. One of my best tricks was to make a personalized bracelet that serves as a reminder of my commitment.I wore that thing for almost a year. ( made it too snug) It works so well that you wont even be able to think about cigarettes without taking note of it. More importantly, If you chose to DO pick up a cigarette. That bracelet reminds you that if you do, its your own damn fault. NO EXCUSES.\r\nWhere I learned these tricks among others was from a book called Carr\'s easyway to stop smoking. The bracelet thing I\'m not sure if that was explicitly from that book, but learned a basic idea of it 20 years before I quit lol.\r\n',15,'2016-10-26T08:03:31.132'),(15,15,'Hey, I think I understand and have been through a similar thing. I\'ve made it to three months several times and then I tend to cave. I\'m trying to work out how I can overcome that this time. I have the same thing about romanticising smoking and I like the smell of smoke too. I suppose it comes down to reminding yourself of the negatives and weighing up whether you think it\'s worth it or not. Throughout my twenties it seemed to work for me. However, I\'m almost thirty now and I notice the effects more such as a chronic cough and shorter breath. I wish I could just have a cig now and then but it just doesn\'t work like that.',16,'2016-10-26T08:05:23.265'),(16,12,'It\'s excellent that you\'re thinking about quitting. It sounds like the reason you started was to find a way in with your peers. The connections you\'ve made with the people you stand around talking to aren\'t going to disappear because you stop smoking... when I quit vaping it took the smokers 2 months to notice.',21,'2016-10-26T08:06:25.614');
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `SETID` int(11) NOT NULL,
  `Setting 1` varchar(150) NOT NULL,
  `User_UID` int(11) NOT NULL,
  PRIMARY KEY (`SETID`),
  UNIQUE KEY `SETID_UNIQUE` (`SETID`),
  KEY `fk_Setting_User1_idx` (`User_UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyanswer`
--

DROP TABLE IF EXISTS `surveyanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyanswer` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `User_UID` int(11) NOT NULL,
  `SurveyQuestion_QID` int(11) NOT NULL DEFAULT '1',
  `Answer` varchar(500) DEFAULT NULL,
  `Ans_date` datetime DEFAULT NULL,
  `answerdate` varchar(255) DEFAULT NULL,
  `cigarettesmoked` bigint(20) DEFAULT NULL,
  `packbought` bigint(20) DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`AID`,`User_UID`,`SurveyQuestion_QID`),
  UNIQUE KEY `AID_UNIQUE` (`AID`),
  KEY `fk_SurveyAnswer_User_idx` (`User_UID`),
  KEY `fk_SurveyAnswer_SurveyQuestion1_idx` (`SurveyQuestion_QID`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyanswer`
--

LOCK TABLES `surveyanswer` WRITE;
/*!40000 ALTER TABLE `surveyanswer` DISABLE KEYS */;
INSERT INTO `surveyanswer` VALUES (1,11,1,NULL,NULL,'20160701',24,3,0),(2,11,1,NULL,NULL,'20160702',19,2,0),(3,11,1,NULL,NULL,'20160703',14,0,0),(4,11,1,NULL,NULL,'20160704',23,4,0),(5,11,1,NULL,NULL,'20160705',12,3,0),(6,11,1,NULL,NULL,'20160706',18,2,0),(7,11,1,NULL,NULL,'20160707',14,0,0),(8,11,1,NULL,NULL,'20160708',24,2,0),(9,11,1,NULL,NULL,'20160709',12,2,0),(10,11,1,NULL,NULL,'20160710',17,0,0),(11,11,1,NULL,NULL,'20160711',12,3,0),(12,11,1,NULL,NULL,'20160712',22,2,0),(13,11,1,NULL,NULL,'20160713',24,0,0),(14,11,1,NULL,NULL,'20160714',19,4,0),(15,11,1,NULL,NULL,'20160715',14,2,0),(16,11,1,NULL,NULL,'20160716',17,2,0),(17,11,1,NULL,NULL,'20160717',13,3,0),(18,11,1,NULL,NULL,'20160718',14,0,0),(19,11,1,NULL,NULL,'20160719',14,3,0),(20,11,1,NULL,NULL,'20160720',19,2,0),(21,11,1,NULL,NULL,'20160721',23,0,0),(22,11,1,NULL,NULL,'20160722',24,4,0),(23,11,1,NULL,NULL,'20160723',18,2,0),(24,11,1,NULL,NULL,'20160724',22,0,0),(25,11,1,NULL,NULL,'20160725',19,0,0),(26,11,1,NULL,NULL,'20160726',23,6,0),(27,11,1,NULL,NULL,'20160727',19,2,0),(28,11,1,NULL,NULL,'20160728',22,2,0),(29,11,1,NULL,NULL,'20160729',14,1,0),(30,11,1,NULL,NULL,'20160730',12,0,0),(31,11,1,NULL,NULL,'20160801',25,3,0),(32,11,1,NULL,NULL,'20160802',16,1,0),(33,11,1,NULL,NULL,'20160803',15,1,0),(34,11,1,NULL,NULL,'20160804',20,4,0),(35,11,1,NULL,NULL,'20160805',12,3,0),(36,11,1,NULL,NULL,'20160806',18,1,0),(37,11,1,NULL,NULL,'20160807',14,0,0),(38,11,1,NULL,NULL,'20160808',24,2,0),(39,11,1,NULL,NULL,'20160809',11,1,0),(40,11,1,NULL,NULL,'20160810',17,0,0),(41,11,1,NULL,NULL,'20160811',12,3,0),(42,11,1,NULL,NULL,'20160812',21,1,0),(43,11,1,NULL,NULL,'20160813',24,0,0),(44,11,1,NULL,NULL,'20160814',16,5,0),(45,11,1,NULL,NULL,'20160815',15,1,0),(46,11,1,NULL,NULL,'20160816',17,2,0),(47,11,1,NULL,NULL,'20160817',13,4,0),(48,11,1,NULL,NULL,'20160818',15,0,0),(49,11,1,NULL,NULL,'20160819',15,3,0),(50,11,1,NULL,NULL,'20160820',16,1,0),(51,11,1,NULL,NULL,'20160821',20,0,0),(52,11,1,NULL,NULL,'20160822',25,4,0),(53,11,1,NULL,NULL,'20160823',18,2,0),(54,11,1,NULL,NULL,'20160824',21,0,0),(55,11,1,NULL,NULL,'20160825',16,0,0),(56,11,1,NULL,NULL,'20160826',20,6,0),(57,11,1,NULL,NULL,'20160827',16,2,0),(58,11,1,NULL,NULL,'20160828',21,1,0),(59,11,1,NULL,NULL,'20160829',15,2,0),(60,11,1,NULL,NULL,'20160830',12,0,0),(61,11,1,NULL,NULL,'20160831',10,1,0),(62,11,1,NULL,NULL,'20160901',25,3,0),(63,11,1,NULL,NULL,'20160902',19,1,0),(64,11,1,NULL,NULL,'20160903',15,0,0),(65,11,1,NULL,NULL,'20160904',22,5,0),(66,11,1,NULL,NULL,'20160905',12,3,0),(67,11,1,NULL,NULL,'20160906',18,1,0),(68,11,1,NULL,NULL,'20160907',14,0,0),(69,11,1,NULL,NULL,'20160908',24,2,0),(70,11,1,NULL,NULL,'20160909',11,1,0),(71,11,1,NULL,NULL,'20160910',17,0,0),(72,11,1,NULL,NULL,'20160911',12,3,0),(73,11,1,NULL,NULL,'20160912',20,1,0),(74,11,1,NULL,NULL,'20160913',24,0,0),(75,11,1,NULL,NULL,'20160914',19,5,0),(76,11,1,NULL,NULL,'20160915',15,1,0),(77,11,1,NULL,NULL,'20160916',17,2,0),(78,11,1,NULL,NULL,'20160917',13,3,0),(79,11,1,NULL,NULL,'20160918',15,0,0),(80,11,1,NULL,NULL,'20160919',15,3,0),(81,11,1,NULL,NULL,'20160920',19,1,0),(82,11,1,NULL,NULL,'20160921',22,0,0),(83,11,1,NULL,NULL,'20160922',25,4,0),(84,11,1,NULL,NULL,'20160923',18,2,0),(85,11,1,NULL,NULL,'20160924',20,0,0),(86,11,1,NULL,NULL,'20160925',16,0,0),(87,11,1,NULL,NULL,'20160926',22,6,0),(88,11,1,NULL,NULL,'20160927',19,1,0),(89,11,1,NULL,NULL,'20160928',20,1,0),(90,11,1,NULL,NULL,'20160929',15,2,0),(91,11,1,NULL,NULL,'20160930',12,0,0),(92,11,1,NULL,NULL,'20161001',22,1,0),(93,11,1,NULL,NULL,'20161002',15,3,0),(94,11,1,NULL,NULL,'20161003',18,1,0),(95,11,1,NULL,NULL,'20161004',22,4,0),(96,11,1,NULL,NULL,'20161005',18,1,0),(97,11,1,NULL,NULL,'20161006',22,5,0),(98,11,1,NULL,NULL,'20161007',19,1,0),(99,11,1,NULL,NULL,'20161008',22,5,0),(100,11,1,NULL,NULL,'20161009',18,1,0),(101,11,1,NULL,NULL,'20161010',11,0,0),(102,11,1,NULL,NULL,'20161011',15,3,0),(103,11,1,NULL,NULL,'20161012',13,0,0),(104,11,1,NULL,NULL,'20161013',12,3,0),(105,11,1,NULL,NULL,'20161014',22,5,0),(106,11,1,NULL,NULL,'20161015',18,1,0),(107,11,1,NULL,NULL,'20161016',11,2,0),(108,11,1,NULL,NULL,'20161017',15,3,0),(109,11,1,NULL,NULL,'20161018',13,0,0),(110,11,1,NULL,NULL,'20161019',12,3,0),(111,11,1,NULL,NULL,'20161020',12,2,0),(112,11,1,NULL,NULL,'20161021',12,2,0),(113,11,1,NULL,NULL,'20161022',17,1,0),(114,11,1,NULL,NULL,'20161023',9,2,0),(115,11,1,NULL,NULL,'20161024',18,3,0),(116,11,1,NULL,NULL,'20161025',12,1,0),(117,13,1,NULL,NULL,'20160701',24,3,0),(118,13,1,NULL,NULL,'20160702',19,2,0),(119,13,1,NULL,NULL,'20160703',14,0,0),(120,13,1,NULL,NULL,'20160704',23,4,0),(121,13,1,NULL,NULL,'20160705',12,3,0),(122,13,1,NULL,NULL,'20160706',18,2,0),(123,13,1,NULL,NULL,'20160707',14,0,0),(124,13,1,NULL,NULL,'20160708',24,2,0),(125,13,1,NULL,NULL,'20160709',12,2,0),(126,13,1,NULL,NULL,'20160710',17,0,0),(127,13,1,NULL,NULL,'20160711',12,3,0),(128,13,1,NULL,NULL,'20160712',22,2,0),(129,13,1,NULL,NULL,'20160713',24,0,0),(130,13,1,NULL,NULL,'20160714',19,4,0),(131,13,1,NULL,NULL,'20160715',14,2,0),(132,13,1,NULL,NULL,'20160716',17,2,0),(133,13,1,NULL,NULL,'20160717',13,3,0),(134,13,1,NULL,NULL,'20160718',14,0,0),(135,13,1,NULL,NULL,'20160719',14,3,0),(136,13,1,NULL,NULL,'20160720',19,2,0),(137,13,1,NULL,NULL,'20160721',23,0,0),(138,13,1,NULL,NULL,'20160722',24,4,0),(139,13,1,NULL,NULL,'20160723',18,2,0),(140,13,1,NULL,NULL,'20160724',22,0,0),(141,13,1,NULL,NULL,'20160725',19,0,0),(142,13,1,NULL,NULL,'20160726',23,6,0),(143,13,1,NULL,NULL,'20160727',19,2,0),(144,13,1,NULL,NULL,'20160728',22,2,0),(145,13,1,NULL,NULL,'20160729',14,1,0),(146,13,1,NULL,NULL,'20160730',12,0,0),(147,13,1,NULL,NULL,'20160801',25,3,0),(148,13,1,NULL,NULL,'20160802',16,1,0),(149,13,1,NULL,NULL,'20160803',15,1,0),(150,13,1,NULL,NULL,'20160804',20,4,0),(151,13,1,NULL,NULL,'20160805',12,3,0),(152,13,1,NULL,NULL,'20160806',18,1,0),(153,13,1,NULL,NULL,'20160807',14,0,0),(154,13,1,NULL,NULL,'20160808',24,2,0),(155,13,1,NULL,NULL,'20160809',11,1,0),(156,13,1,NULL,NULL,'20160810',17,0,0),(157,13,1,NULL,NULL,'20160811',12,3,0),(158,13,1,NULL,NULL,'20160812',21,1,0),(159,13,1,NULL,NULL,'20160813',24,0,0),(160,13,1,NULL,NULL,'20160814',16,5,0),(161,13,1,NULL,NULL,'20160815',15,1,0),(162,13,1,NULL,NULL,'20160816',17,2,0),(163,13,1,NULL,NULL,'20160817',13,4,0),(164,13,1,NULL,NULL,'20160818',15,0,0),(165,13,1,NULL,NULL,'20160819',15,3,0),(166,13,1,NULL,NULL,'20160820',16,1,0),(167,13,1,NULL,NULL,'20160821',20,0,0),(168,13,1,NULL,NULL,'20160822',25,4,0),(169,13,1,NULL,NULL,'20160823',18,2,0),(170,13,1,NULL,NULL,'20160824',21,0,0),(171,13,1,NULL,NULL,'20160825',16,0,0),(172,13,1,NULL,NULL,'20160826',20,6,0),(173,13,1,NULL,NULL,'20160827',16,2,0),(174,13,1,NULL,NULL,'20160828',21,1,0),(175,13,1,NULL,NULL,'20160829',15,2,0),(176,13,1,NULL,NULL,'20160830',12,0,0),(177,13,1,NULL,NULL,'20160831',10,1,0),(178,13,1,NULL,NULL,'20160901',25,3,0),(179,13,1,NULL,NULL,'20160902',19,1,0),(180,13,1,NULL,NULL,'20160903',15,0,0),(181,13,1,NULL,NULL,'20160904',22,5,0),(182,13,1,NULL,NULL,'20160905',12,3,0),(183,13,1,NULL,NULL,'20160906',18,1,0),(184,13,1,NULL,NULL,'20160907',14,0,0),(185,13,1,NULL,NULL,'20160908',24,2,0),(186,13,1,NULL,NULL,'20160909',11,1,0),(187,13,1,NULL,NULL,'20160910',17,0,0),(188,13,1,NULL,NULL,'20160911',12,3,0),(189,13,1,NULL,NULL,'20160912',20,1,0),(190,13,1,NULL,NULL,'20160913',24,0,0),(191,13,1,NULL,NULL,'20160914',19,5,0),(192,13,1,NULL,NULL,'20160915',15,1,0),(193,13,1,NULL,NULL,'20160916',17,2,0),(194,13,1,NULL,NULL,'20160917',13,3,0),(195,13,1,NULL,NULL,'20160918',15,0,0),(196,13,1,NULL,NULL,'20160919',15,3,0),(197,13,1,NULL,NULL,'20160920',19,1,0),(198,13,1,NULL,NULL,'20160921',22,0,0),(199,13,1,NULL,NULL,'20160922',25,4,0),(200,13,1,NULL,NULL,'20160923',18,2,0),(201,13,1,NULL,NULL,'20160924',20,0,0),(202,13,1,NULL,NULL,'20160925',16,0,0),(203,13,1,NULL,NULL,'20160926',22,6,0),(204,13,1,NULL,NULL,'20160927',19,1,0),(205,13,1,NULL,NULL,'20160928',20,1,0),(206,13,1,NULL,NULL,'20160929',15,2,0),(207,13,1,NULL,NULL,'20160930',12,0,0),(208,13,1,NULL,NULL,'20161001',22,1,0),(209,13,1,NULL,NULL,'20161002',15,3,0),(210,13,1,NULL,NULL,'20161003',18,1,0),(211,13,1,NULL,NULL,'20161004',22,4,0),(212,13,1,NULL,NULL,'20161005',18,1,0),(213,13,1,NULL,NULL,'20161006',22,5,0),(214,13,1,NULL,NULL,'20161007',19,1,0),(215,13,1,NULL,NULL,'20161008',22,5,0),(216,13,1,NULL,NULL,'20161009',18,1,0),(217,13,1,NULL,NULL,'20161010',11,0,0),(218,13,1,NULL,NULL,'20161011',15,3,0),(219,13,1,NULL,NULL,'20161012',13,0,0),(220,13,1,NULL,NULL,'20161013',12,3,0),(221,13,1,NULL,NULL,'20161014',22,5,0),(222,13,1,NULL,NULL,'20161015',18,1,0),(223,13,1,NULL,NULL,'20161016',11,2,0),(224,13,1,NULL,NULL,'20161017',15,3,0),(225,13,1,NULL,NULL,'20161018',13,0,0),(226,13,1,NULL,NULL,'20161019',12,3,0),(227,13,1,NULL,NULL,'20161020',12,2,0),(228,13,1,NULL,NULL,'20161021',12,2,0),(229,13,1,NULL,NULL,'20161022',17,1,0),(230,13,1,NULL,NULL,'20161023',9,2,0),(231,13,1,NULL,NULL,'20161024',18,3,0),(232,13,1,NULL,NULL,'20161025',12,1,0);
/*!40000 ALTER TABLE `surveyanswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestion`
--

DROP TABLE IF EXISTS `surveyquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestion` (
  `QID` int(11) NOT NULL AUTO_INCREMENT,
  `Question` varchar(800) DEFAULT NULL,
  `AnswerType` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`QID`),
  UNIQUE KEY `QID_UNIQUE` (`QID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestion`
--

LOCK TABLES `surveyquestion` WRITE;
/*!40000 ALTER TABLE `surveyquestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `UID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `password` varchar(500) NOT NULL,
  `email` varchar(150) NOT NULL,
  `role` tinyint(1) unsigned NOT NULL COMMENT '0:admin;1:smoker;2:gp',
  `phonenum` varchar(45) DEFAULT NULL,
  `date_of_birth` varchar(100) DEFAULT NULL,
  `created_date` varchar(100) DEFAULT NULL,
  `gp_license` varchar(45) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'gp:0:pending;1:approval;2:denied;smoker:0:normal,1:forbidden',
  `receive_email` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0:yes;1:no',
  `aboutme` text,
  `gpid` bigint(20) DEFAULT '0',
  PRIMARY KEY (`UID`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `UID_UNIQUE` (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (11,'jackquit','Jack','Quit','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'0483990223','1980-03-23','20161020','',0,0,NULL,0),(12,'saveworld','Eric','Miller','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',2,'0499839293','1970-09-09','20161020','213213213',1,0,NULL,0),(13,'extraclean','Tom','Cat','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'0482384444','1989-10-13','20161023','',0,0,NULL,12),(14,'admin','admin','admin','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',0,'0451882798','2016-09-28','20161024','',0,0,NULL,0),(15,'test123','user','test','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'0432887799','2016-10-06','20161024','',0,0,NULL,0),(16,'baoming','bao','ming','2ac9cb7dc02b3c0083eb70898e549b63','vincentdenghui@gmail.com',1,'0450888888','2016-10-06									','20161025','',0,0,'I am a loser programmer! yahye....',0),(17,'taolin','taolin','jin','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'0451882798','2016-10-26','20161025','',0,0,NULL,12),(18,'anothertj','tao','jin','2ac9cb7dc02b3c0083eb70898e549b63','tjin0016@uni.sydney.edu.au',1,'0456333333','1996-10-23','20161026','',1,0,NULL,0),(19,'gpkevin','Kevin','Zero','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',2,'0321543231','1969-10-18','20161026','43246523',0,0,NULL,0),(20,'robinz','robin','zh','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'0478998333','1980-10-05','20161026','',0,0,NULL,0),(21,'trte9090','tray','teke','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',1,'4093898080','2016-10-05','20161026','',1,0,NULL,0),(23,'fakegp','GP','Fake','2ac9cb7dc02b3c0083eb70898e549b63','taolinjin@gmail.com',2,'0000000000','1999-10-21','20161026','00000000',2,0,NULL,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-26 10:26:14
