package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.Board;

public interface IBoardManager extends Serializable {

	public List<Board> getBoards();

	public String getBoardNameByboardId(int boardId);

	public void addBoard(Board board);

	public void delBoard(Board board);

	public List<Board> getBoardByBoardName(String boardName);
	
	public Board getBoardByBoardId(int boardId);

}
