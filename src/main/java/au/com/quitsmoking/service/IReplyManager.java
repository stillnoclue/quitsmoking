package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.Reply;

public interface IReplyManager extends Serializable {

	public List<Reply> getRepliesByPostId(int postId);
	public Reply newReply(int author, String content, int postId);
	public List<Reply> getReplies(int postId);
	public Reply getReplyByReplyId(int replyId);

}
