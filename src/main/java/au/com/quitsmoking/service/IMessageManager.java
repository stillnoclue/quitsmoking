package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.Message;

public interface IMessageManager extends Serializable{

	public void markAsRead(int id);
	public void markAsUnread(int id);
	public List<Message> getMessagesToUser(int to);
	public List<Message> getMessagesFromUser(int from);
	public Message getMessageByMessageId(int messageId);
	public Message createMessage(int fromUserId, int toUserId, String subject, String content);
	public int getInboxCount(int userId);
	public int getOutboxCount(int userId);
	
	
}
