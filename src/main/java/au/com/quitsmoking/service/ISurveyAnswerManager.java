
package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.SurveyAnswer;

public interface ISurveyAnswerManager extends Serializable {

	public void addAnswer(SurveyAnswer answer);

	public void updateAnswer(SurveyAnswer answer);

	public SurveyAnswer getTodayAnswer(long uid);
	
	public List<SurveyAnswer> getAnswerList(long uid);
	
	public SurveyAnswer getRecordForThisUserAndThisDate(long uid, String date);
}
