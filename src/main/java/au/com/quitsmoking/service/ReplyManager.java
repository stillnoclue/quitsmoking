package au.com.quitsmoking.service;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.Reply;


@Service(value = "replyManager")
@Transactional
public class ReplyManager implements IReplyManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Autowired
	private SessionFactory sessionFactory;

	
	@Override
	public Reply getReplyByReplyId(int replyId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Reply theReply = (Reply) currentSession.get(Reply.class, replyId);
		return theReply;
	}

	@Override
	public List<Reply> getRepliesByPostId(int postId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Reply where postId =:id");
		query.setParameter("id", postId);
		List<Reply> list = query.list();
		return list;
	}

	@Override
	public Reply newReply(int author, String content, int postId) {
		Session currentSession = sessionFactory.getCurrentSession();
		LocalDateTime time = LocalDateTime.now();

		Reply newReply = new Reply(author,content,time.toString(), postId);

		currentSession.save(newReply);
		
		return newReply;
	}
	
	@Override
	public List<Reply> getReplies(int postId) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		// create a query
		Query query = currentSession.createQuery("from Reply where postId=:postId");
		query.setParameter("postId", postId);
		// execute query and get the result list
		List<Reply> replies = query.list();
		
		//return the result list
		return replies;

	}

}
