package au.com.quitsmoking.service;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.Message;

@Service(value="messageManager")
@Transactional
public class MessageManager implements IMessageManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Message createMessage(int fromUserId, int toUserId, String subject, String content) {
		Session currentSession = sessionFactory.getCurrentSession();
		LocalDateTime time = LocalDateTime.now();
		Message theMessage = new Message(fromUserId, toUserId,subject, content, time.toString());
		currentSession.save(theMessage);
		return theMessage;
	}

	@Override
	public void markAsRead(int messageId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Message theMessage = (Message) currentSession.get(Message.class, messageId);
		theMessage.setRead(1);
	}

	@Override
	public void markAsUnread(int messageId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Message theMessage = (Message) currentSession.get(Message.class, messageId);
		theMessage.setRead(0);
	}

	@Override
	public List<Message> getMessagesToUser(int toUserId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Message where toUserId=:toUserId order by time desc");
		query.setParameter("toUserId", toUserId);
		List<Message> messages = query.list();
		return messages;
	}

	@Override
	public List<Message> getMessagesFromUser(int fromUserId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Message where fromUserId=:fromUserId order by time desc");
		query.setParameter("fromUserId", fromUserId);
		List<Message> messages = query.list();
		return messages;
	}

	@Override
	public Message getMessageByMessageId(int messageId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Message theMessage = (Message) currentSession.get(Message.class, messageId);
		
		return theMessage;
	}
	
	@Override
	public int getInboxCount(int userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Message where toUserId=:toUserId and ifRead = 0");
		query.setParameter("toUserId", userId);
		if (query.list() != null){
			return query.list().size();
		}else{
			return 0;
		}

	}
	
	@Override
	public int getOutboxCount(int fromUserId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Message where fromUserId=:fromUserId and ifRead = 0");
		query.setParameter("fromUserId", fromUserId);
		if (query.list() != null){
			return query.list().size();
		}else{
			return 0;
		}

	}
	
}
