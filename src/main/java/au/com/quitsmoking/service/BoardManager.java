package au.com.quitsmoking.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.Board;


@Service(value = "boardManager")
@Transactional
public class BoardManager implements IBoardManager {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Board> getBoards() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Board");
		List<Board> list = query.list();
		return list;
	}

	@Override
	public String getBoardNameByboardId(int boardId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Board theBoard = (Board) currentSession.get(Board.class, boardId);
		return theBoard.getBoardName();
	}

	@Override
	public List<Board> getBoardByBoardName(String boardName) {
		StringBuffer sb = new StringBuffer();
		sb.append("from Board where board_name=:board_name");
		Query query = this.sessionFactory.getCurrentSession().createQuery(sb.toString());
		query.setParameter("board_name", boardName);
		return query.list();
	}
	
	@Override
	public Board getBoardByBoardId(int boardId) {
		StringBuffer sb = new StringBuffer();
		sb.append("from  Board where board_id=:board_id");
		Query query = this.sessionFactory.getCurrentSession().createQuery(sb.toString());
		query.setParameter("board_id", boardId);
		return (Board) query.uniqueResult();
	}
	
	@Transactional(rollbackFor = {Exception.class}, readOnly = false)
	public void addBoard(Board board) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(board);
	}

	@Override
	public void delBoard(Board board) {
		this.sessionFactory.getCurrentSession().delete(board);
	}
	
}
