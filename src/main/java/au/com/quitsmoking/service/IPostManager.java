package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.Post;

public interface IPostManager extends Serializable {

	public Post getPostById(int id);
	public List<Post> getPostsByBoardId(int id);
	public Post newPost(String content, int author, String topic, int boardId);
	public int getNumberOfRepliesByPostId(int id);
	public int getNumberOfPostsByPostId(int id);
	public Post getLatestPostByBoardId(int id);
	public List<Post> getTenLatestPosts();
}
