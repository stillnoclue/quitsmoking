package au.com.quitsmoking.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.User;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public List<User> getUserList() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
	}

	@Override
	public List<User> getEmailUserList() {
		String sql = "from User where receive_email=0";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List l = query.list();
		return l;
	}
	
	@Transactional(rollbackFor = { Exception.class }, readOnly = false)
	public void addUser(User user) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String today = df.format(date);
		user.setCreated_date(today);
		this.sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public User getUserByUsername(String username) {
		StringBuffer sb = new StringBuffer();
		sb.append("from  User  where username=:username");
		Query query = this.sessionFactory.getCurrentSession().createQuery(sb.toString());
		query.setParameter("username", username);
		User user = (User) query.list().get(0);
		return user;
	}

	public boolean checkLoginData(String username, String password) {
		String sql = "from User where username=:username and password=:password";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("username", username);
		query.setParameter("password", password);
		List l = query.list();
		if (l.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public User getUserByUID(long uid) {
		String sql = "from User where uid=:uid";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("uid", uid);
		User user = (User) query.list().get(0);
		return user;
	}

	@Override
	public void updateUser(User user) {
		this.sessionFactory.getCurrentSession().merge(user);

	}

	@Override
	public List<User> getGPList() {
		String sql = "from User where role = 2";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List<User> list = query.list();
		return list;
	}

	@Override
	public List<User> getPendingGPList() {
		String sql = "from User where role = 2 and status = 0";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List<User> list = query.list();
		return list;
	}
	
	@Override
	public List<User> getAllUserList() {
		String sql = "from User where role = 1";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List<User> list = query.list();
		return list;
	}
	
	@Override
	public User getGPForThisUser(User user) {
		String sql = "from User where uid=:gpid";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("gpid", user.getGpid());
		List<User> list = query.list();
		User gp=null;
		if(list != null){
			if(list.size() > 0){
				gp = (User) list.get(0);
			}
		}
		return gp;
	}

	@Override
	public List<User> getPatientList(long gpid) {
		String sql = "from User where gpid=:gpid";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("gpid", gpid);
		List<User> list = query.list();
		return list;
	}

	@Override
	public boolean checkUserNameAlreadyExisted(String username) {
		String sql = "from User where username=:username";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("username", username);
		List l = query.list();
		if(l != null){
			if(l.size() > 0){
				return true;
			}
		}
		return false;
	}

	@Override
	public List<User> searchMembers(String keyword) {
		String sql = "Select distinct u from User u where u.username like :keyword OR u.firstname like :keyword OR u.lastname like :keyword";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("keyword", keyword+"%");
		List<User> l = query.list();
		return l;
	}

	@Override
	public void approve(User user) { 
		this.sessionFactory.getCurrentSession().update(user); 
	}
	
	@Override
	public List<User> login(String username, String pwd) {
		StringBuffer sb = new StringBuffer();
		sb.append("from  User  where username='"+username+"' and password= '"+pwd+"' ");
		Query query = this.sessionFactory.getCurrentSession().createQuery(sb.toString()); 
		return query.list(); 
	}

}