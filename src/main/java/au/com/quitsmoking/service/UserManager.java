package au.com.quitsmoking.service;

import org.hibernate.SessionFactory;
import org.hibernate.annotations.Target;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.User;

@Service(value = "userManager")
@Transactional
public class UserManager implements IUserManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);

	}

	@Override
	public void updateUser(User user) {
		this.sessionFactory.getCurrentSession().merge(user);
	}

	@Override
	public User getUserById(long id) {
		User user = (User) this.sessionFactory.getCurrentSession().get(User.class, id);
		return user;
	}

}
