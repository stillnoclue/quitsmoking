
package au.com.quitsmoking.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.SurveyAnswer;

@Service(value = "surveyAnswerManager")
@Transactional
public class SurveyAnswerManager implements ISurveyAnswerManager {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addAnswer(SurveyAnswer answer) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String today = df.format(date);
		answer.setAnswerdate(today);
		this.sessionFactory.getCurrentSession().save(answer);
	}

	@Override
	public void updateAnswer(SurveyAnswer answer) {
		this.sessionFactory.getCurrentSession().merge(answer);
	}

	@Override
	public SurveyAnswer getTodayAnswer(long uid) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String today = df.format(date);
		String sql = "from SurveyAnswer where user_uid=:uid And answerdate=:date";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("uid", uid);
		query.setParameter("date", today);
		List l = query.list();
		SurveyAnswer answer = new SurveyAnswer();
		if (l != null) {
			if (l.size() > 0) {
				answer = (SurveyAnswer) l.get(0);
			}
		}
		return answer;
	}
	
	@Override
	public List<SurveyAnswer> getAnswerList(long uid) {
		String sql = "from SurveyAnswer where user_uid=:uid";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("uid", uid);
		List<SurveyAnswer> answerList = query.list();
		return answerList;
	}

	@Override
	public SurveyAnswer getRecordForThisUserAndThisDate(long uid, String date) {
		String sql = "from SurveyAnswer where user_uid=:uid and answerdate=:date";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("uid", uid);
		query.setParameter("date", date);
		List<SurveyAnswer> answerList = query.list();
		SurveyAnswer ans = null;
		if(answerList != null){
			if(answerList.size() > 0){
				ans = answerList.get(0);
			}
		}
		return ans;
	}

}
