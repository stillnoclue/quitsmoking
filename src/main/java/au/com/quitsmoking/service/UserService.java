package au.com.quitsmoking.service;

import java.io.Serializable;
import java.util.List;

import au.com.quitsmoking.domain.User;

public interface UserService extends Serializable {

	public List<User> getUserList();

	public void addUser(User user);

	public User getUserByUsername(String username);

	public boolean checkLoginData(String username, String password);

	public User getUserByUID(long uid);

	public void updateUser(User user);

	public List<User> getGPList();

	public User getGPForThisUser(User user);
	
	public List<User> getPatientList(long gpid);
	
	public boolean checkUserNameAlreadyExisted(String username);
	
	public List<User> getPendingGPList();
	
	public List<User> searchMembers(String keyword);
	
	public List<User> getAllUserList();
	
    public void approve(User user); 
    
    public List<User> login(String username,String pwd);

	public List<User> getEmailUserList();

}