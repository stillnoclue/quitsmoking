package au.com.quitsmoking.service;

import java.io.Serializable;

import au.com.quitsmoking.domain.User;

public interface IUserManager extends Serializable {

	public void addUser(User user);

	public void updateUser(User user);

	public User getUserById(long id);

}
