package au.com.quitsmoking.service;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.Reply;



@Service(value = "postManager")
@Transactional
public class PostManager implements IPostManager {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Post getPostById(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Post thePost = (Post) currentSession.get(Post.class, id);
		return thePost;
	}
	@Override
	public List<Post> getPostsByBoardId (int id){
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Post where boardId =:id");
		query.setParameter("id", id);
		List<Post> list = query.list();
		return list;
	}
	
	@Override
	public Post newPost(String content, int author, String topic, int boardId){
		
		Session currentSession = sessionFactory.getCurrentSession();
		LocalDateTime time = LocalDateTime.now();

		Post newPost = new Post(content, time.toString(), author, topic, boardId);

		// save the newPost
		currentSession.save(newPost);
		
		return newPost;
		
	}
	@Override
	public int getNumberOfRepliesByPostId(int id){
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Reply where postId =:id");
		query.setParameter("id", id);
		List<Reply> list = query.list();
		if (list != null){
			return list.size();
		}else
		{
			return 0;
		}
	}
	
	@Override
	public int getNumberOfPostsByPostId(int id){
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Post where boardId =:id");
		query.setParameter("id", id);
		List<Reply> list = query.list();
		if (list != null){
			return list.size();
		}else
		{
			return 0;
		}
	}
	@Override
	public Post getLatestPostByBoardId(int id){
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Post where boardId =:id");
		query.setParameter("id", id);
		List<Post> list = query.list();
		if (list != null){
			return list.get(list.size()-1);
		}else
		{
			return null;
		}
	}
	@Override
	public List<Post> getTenLatestPosts() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from Post order by time desc");
		query.setMaxResults(10);
		
		return query.list();
	}
}
