package au.com.quitsmoking.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.UserService;

@Controller
public class SearchController {
	@Autowired
	private UserService userservice;
	
	@RequestMapping("/search")
	public String search(Model model, HttpServletRequest request, @RequestParam String keyword){
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<User> userList = this.userservice.searchMembers(keyword);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("userList",userList);
		model.addAttribute("keyword", keyword);
		return "search";
	}
}
