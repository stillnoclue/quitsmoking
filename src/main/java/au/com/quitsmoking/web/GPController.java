package au.com.quitsmoking.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.UserService;

@Controller
public class GPController {
	
	@Autowired
	private UserService userservice;
	
	@RequestMapping(value = "/gphome")
	public String gphome(Model model, HttpServletRequest request) {
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<User> patientList = this.userservice.getPatientList(loginUser.getUid());
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("patientList",patientList);
		return "gphome";
	}
}
