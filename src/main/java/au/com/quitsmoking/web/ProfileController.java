package au.com.quitsmoking.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.util.PasswordHash;

@Controller
public class ProfileController {
	
	@Autowired
	private UserService userservice;
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String profile(HttpServletRequest httpServletRequest, Model model) {
		User profileUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		model.addAttribute("profileUser", profileUser);
		model.addAttribute("loginUser", profileUser);
		return "profile";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile_get(HttpServletRequest httpServletRequest, Model model,@RequestParam(required=false) Long uid) {
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		User profileUser = loginUser;
		if(uid != null){
			profileUser = this.userservice.getUserByUID(uid);
		}

		model.addAttribute("profileUser", profileUser);
		model.addAttribute("loginUser", loginUser);
		return "profile";
	
	}

	@RequestMapping(value = "/editprofile", method = RequestMethod.GET)
	public String editProfile(HttpServletRequest httpServletRequest, Model model) {
		User profileUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		model.addAttribute("profileUser", profileUser);
		model.addAttribute("loginUser", profileUser);
		return "editprofile";
	}

	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public String updateUser(HttpServletRequest httpServletRequest, @ModelAttribute User user, Model model) {
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		loginUser.setFirstname(user.getFirstname());
		loginUser.setLastname(user.getLastname());
		loginUser.setEmail(user.getEmail());
		loginUser.setPhonenum(user.getPhonenum());
		loginUser.setDate_of_birth(user.getDate_of_birth());
		loginUser.setAboutme(user.getAboutme());
		if (user.getGp_license() != null) {
			if (user.getGp_license().trim() != "") {
				loginUser.setGp_license(user.getGp_license());
			}
		}
		this.userservice.updateUser(loginUser);
		httpServletRequest.getSession().setAttribute("loginUser", loginUser);
		model.addAttribute("profileUser", loginUser);
		return "profile";
	}
	
	@RequestMapping(value ="/changePassword", method=RequestMethod.POST)
	public String changePassword(Model model, HttpServletRequest request, @RequestParam String oldPassword, @RequestParam String newPassword){
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		try {
			if(PasswordHash.validatePassword(oldPassword, loginUser.getPassword())){
				String passwordHash = PasswordHash.createHash(newPassword);
				loginUser.setPassword(passwordHash);
				this.userservice.updateUser(loginUser);
			}else{
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "Password is incorrect.");
			}
		} catch (NoSuchAlgorithmException e) {
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Password is incorrect.");
		} catch (InvalidKeySpecException e) {
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Password is incorrect.");
		}
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("profileUser", loginUser);
		return "editprofile";
	}
	
}
