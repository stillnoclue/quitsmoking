package au.com.quitsmoking.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.SurveyAnswer;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.IPostManager;
import au.com.quitsmoking.service.ISurveyAnswerManager;
import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.util.AjaxResult;
import au.com.quitsmoking.util.PasswordHash;

@Controller
@RequestMapping({ "/" })
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	// @Resource(name="userService")
	// private UserService userservice;

	@Autowired
	private UserService userservice;
	@Autowired
	private ISurveyAnswerManager surveyAnswerManager;
	@Autowired
	private IPostManager postManager;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		try {
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			CustomDateEditor dateEditor = new CustomDateEditor(fmt, true);
			binder.registerCustomEditor(Date.class, dateEditor);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@RequestMapping({ "/" })
	public String login(HttpServletRequest httpServletRequest, Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest httpServletRequest, Model model, HttpServletResponse response) {
		httpServletRequest.getSession().removeAttribute("loginUser");
		httpServletRequest.getSession().invalidate();
		return "logout";
	}
	
	@RequestMapping(value = "/adminlogout", method = RequestMethod.GET)
	public String adminlogout(HttpServletRequest httpServletRequest, Model model, HttpServletResponse response) {
		httpServletRequest.getSession().removeAttribute("loginUser");
		httpServletRequest.getSession().invalidate();
		return "login";
	}


	@RequestMapping(value = "/verifylogin", method = RequestMethod.POST)
	public String login(@ModelAttribute User user, Model model, HttpServletRequest request) {
		try {
			List<User> list = userservice.login(user.getUsername(), DigestUtils.md5Hex(user.getPassword()));
			if (list.size() > 0) {
				user = list.get(0);
				request.getSession().setAttribute("loginUser", user);
				if (user.getRole() == 1) {
					if (user.getStatus() == 1) {
						model.addAttribute("msgTitle", "Error");
						model.addAttribute("msgContent", "Member Account is locked!");
						return "login";
					} else if (user.getStatus() == 0) {
						SurveyAnswer todayAnswer = this.surveyAnswerManager.getTodayAnswer(user.getUid());
						// create a hashmap for uid-username mapping in the view.
						Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();
						
						List<Post> posts = this.postManager.getTenLatestPosts();
						for(Post thePost : posts){
							// add the post user name map
							uidUsernameMap.put(thePost.getAuthor(), userservice.getUserByUID(thePost.getAuthor()).getUsername());	
						}
						if (todayAnswer == null)
							todayAnswer = new SurveyAnswer();
						model.addAttribute("loginUser", user);
						model.addAttribute("answer", todayAnswer);
						model.addAttribute("posts", posts);
						model.addAttribute("uidUsernameMap",uidUsernameMap);
						return "home";
					}
					
				} else if (user.getRole() == 2) {
					if (user.getStatus() == 0) {
						model.addAttribute("msgTitle", "Error");
						model.addAttribute("msgContent", "GP License is in process of verification! Please return soon.");
						return "login";
					} else if (user.getStatus() == 1) {
						List<User> patientList = this.userservice.getPatientList(user.getUid());
						model.addAttribute("loginUser", user);
						model.addAttribute("patientList",patientList);
						return "gphome";
					} else if (user.getStatus() == 2) {
						model.addAttribute("msgTitle", "Error");
						model.addAttribute("msgContent", "GP License is not approved.");
						return "login";
					}
					
				} else if (user.getRole() == 0) {
					model.addAttribute("loginUser", user);
					return "main";
				}
				return "login";
			} else {
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "Invalid username or password!");
				return "login";
			}
			
		} catch (Exception e) {
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Fail to Login!");
			return "login";
		}
//		return "login";
	}

	
	@RequestMapping({ "/register" })
	public String register(Locale locale, Model model) {
		return "register";
	}

	/*@RequestMapping({ "/saveregister" })
	@ResponseBody
	public AjaxResult saveregister(HttpServletRequest request, @ModelAttribute("user") User user, Model model) {
		AjaxResult ajaxresult = new AjaxResult();
		ajaxresult.setUrl("/");
		try {
			StringBuffer where = new StringBuffer();

			List dbList = userservice.getUserByUsername(user.getUsername());
			if (dbUser != null && dbUser.getUid() != 0) {
				throw new Exception("The username exitst.");
			}
			if (user.getRole() == 1) {
				user.setGp_license("");
			}
			if (user.getRole() == 2) {
				if (user.getGp_license() == null || user.getGp_license().trim().equals("")) {
					throw new Exception("Please provide your GP License for Verification.");
				}
			}
			user.setPassword(DigestUtils.md5Hex(user.getPassword()));
			user.setStatus(0);
			user.setReceive_email(0);
			user.setCreated_date(new Date().toString());
			this.userservice.addUser(user);

			ajaxresult.setSuccess(true);
			ajaxresult.setMsg("Successful!");

		} catch (Exception e) {
			ajaxresult.setSuccess(false);
			ajaxresult.setMsg("Fail to save��" + e.getMessage());
		}
		return ajaxresult;
	}*/

	@RequestMapping({ "/addUser" })
	public String addUser(HttpServletRequest httpServletRequest, @ModelAttribute("user") User user, Model model) throws NoSuchAlgorithmException, InvalidKeySpecException {

		if(this.userservice.checkUserNameAlreadyExisted(user.getUsername())){
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Username is already existed");
			model.addAttribute("user",user);
			return "register";
		}
		user.setPassword(DigestUtils.md5Hex(user.getPassword()));
		user.setStatus(0);
		user.setReceive_email(0);
		user.setCreated_date(new Date().toString());
		if (user.getRole() == 1) {
			user.setGp_license("");
		}
		if (user.getRole() == 2) {
			if (user.getGp_license() == null || user.getGp_license().trim().equals("")) {
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "Please provide your GP License for verification!");
				model.addAttribute("user",user);
				return "register";
			}
		}
		
		this.userservice.addUser(user);
		model.addAttribute("msgTitle", "Success");
		model.addAttribute("msgContent","Successfully register! Welcome~");
		model.addAttribute("user", user);
		return "login";
	}

	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String home_post(HttpServletRequest httpServletRequest, Model model) {
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		SurveyAnswer todayAnswer = this.surveyAnswerManager.getTodayAnswer(loginUser.getUid());
		List<Post> posts = this.postManager.getTenLatestPosts();
		model.addAttribute("posts", posts);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("answer", todayAnswer);
		return "home";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(HttpServletRequest httpServletRequest, Model model) {
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		SurveyAnswer todayAnswer = this.surveyAnswerManager.getTodayAnswer(loginUser.getUid());
		List<Post> posts = this.postManager.getTenLatestPosts();
		model.addAttribute("posts", posts);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("answer", todayAnswer);
		return "home";
	}

	@RequestMapping(value = "/updateSurvey", method = RequestMethod.POST)
	public String updateSurvey(HttpServletRequest httpServletRequest, Model model,
			@ModelAttribute SurveyAnswer answer) {
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		answer.setUser_id(loginUser.getUid());
		SurveyAnswer todayAnswer = this.surveyAnswerManager.getTodayAnswer(loginUser.getUid());
		if (answer.getAnswerdate().equalsIgnoreCase("TBD")) {
			this.surveyAnswerManager.addAnswer(answer);
		} else {
			answer.setId(todayAnswer.getId());
			this.surveyAnswerManager.updateAnswer(answer);
		}
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("answer", answer);
		return "home";
	}

	
	@RequestMapping(value = "/assigngp")
	public String assignGp(Model model, HttpServletRequest request){
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<User> gpList = this.userservice.getGPList();
		User assignedGp = this.userservice.getGPForThisUser(loginUser);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("gpList", gpList);
		model.addAttribute("assignedGP", assignedGp);
		return "assigngp";
	}
	
	@RequestMapping(value = "/updategp", method=RequestMethod.POST)
	public String updateGp(Model model, HttpServletRequest request,@ModelAttribute User gp){
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		loginUser.setGpid(gp.getUid());
		this.userservice.updateUser(loginUser);
		List<User> gpList = this.userservice.getGPList();
		User assignedGp = this.userservice.getGPForThisUser(loginUser);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("gpList", gpList);
		model.addAttribute("assignedGP", assignedGp);
		return "assigngp";
	}
	
	/**
	 * GP List
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping({"/doctorlist"})
	public String doctorlist(HttpServletRequest request,Model model) { 
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<User> list=userservice.getGPList();	
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("doctorlist",list);
		return "doctorlist";
	}
	
	/**
	 * smoker list
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping({"/smokerlist"})
	public String smokerlist(HttpServletRequest request,Model model) { 
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<User> list=userservice.getAllUserList();
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("smokerlist",list);
		return "smokerlist";
	}
 
	/**
	 * verify gp 
	 * @param request
	 * @param user
	 * @param model
	 * @return
	 */
 	@RequestMapping(value="/approve/{approveval}/{id}")
	@ResponseBody
	public AjaxResult approve(HttpServletRequest request,@PathVariable("approveval") int approveval,@PathVariable("id") long id,  Model model) {
        AjaxResult ajaxresult = new AjaxResult();   
	    ajaxresult.setUrl("/doctorlist");
		try {  
			User user=userservice.getUserByUID(id);
				user.setStatus(approveval);
			 userservice.approve(user);
            ajaxresult.setSuccess(true);  
            ajaxresult.setMsg("Success!");
	    } catch (Exception e) { 
	      ajaxresult.setSuccess(false);
	      ajaxresult.setMsg("Fail to save: " + e.getMessage());
	    }       
		return ajaxresult;
	}
	
	/**
	 * set smoker user status
	 * @param request
	 * @param user
	 * @param model
	 * @return
	 */ 
		@RequestMapping(value="/userstatus/{approveval}/{id}")
		@ResponseBody
		public AjaxResult userstatus(HttpServletRequest request,@PathVariable("approveval") int approveval,@PathVariable("id") long id,  Model model) {
	    
        AjaxResult ajaxresult = new AjaxResult(); 
	    ajaxresult.setUrl("/smokerlist");
		try { 
			User user=userservice.getUserByUID(id);
			user.setStatus(approveval);
			userservice.approve(user);
            ajaxresult.setSuccess(true);  
            ajaxresult.setMsg("Success");
	    } catch (Exception e) { 
	      ajaxresult.setSuccess(false);
	      ajaxresult.setMsg("Fail to save: " + e.getMessage());
	    }       
		return ajaxresult;
	}
		
		/**
		 * send email
		 * @param httpServletRequest
		 * @return
		 */
		@RequestMapping({"/sendemail"})
		public String sendemail(HttpServletRequest httpServletRequest) { 
			return "sendemail";
		}	
		
		
		/**
		 * send email
		 * @param request
		 * @param user
		 * @param model
		 * @return
		 */ 
		@RequestMapping(value="/savesendemail")
		@ResponseBody
		public AjaxResult savesendemail(HttpServletRequest request,@RequestParam("subject") String subject, @RequestParam("content") String content,  Model model) {
	    AjaxResult ajaxresult = new AjaxResult(); 
	    ajaxresult.setUrl("/sendemail");
		try {
			 
		List<User> list=userservice.getUserList(); 
			User user=new User();
		 
			for (int i = 0; i<list.size();i++) {
				user=list.get(i);
				if(user.getReceive_email()==0){			        
		        final String username = "quitsmokingau@gmail.com";  
		        final String password = "elec5619";  
		         
		        
		        Properties props = new Properties();
		        props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.port", "587");
		         
				Session session = Session.getInstance(props,
						  new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(username, password);
							}
						  });
				
				try {

					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(username));
					message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(user.getEmail()));
					message.setSubject(subject);
					message.setText(content);

					Transport.send(message);

					System.out.println("Done");

				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
				}			
			}
		 
            ajaxresult.setSuccess(true);  
            ajaxresult.setMsg("Success");
	    } catch (Exception e) { 
	    	e.printStackTrace();
	      ajaxresult.setSuccess(false);
	      ajaxresult.setMsg("Fail:" + e.getMessage());
	    }      
		return ajaxresult;
	}
		
			
		
	/**
	 * backend system main
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/main"})
	public String main(HttpServletRequest httpServletRequest) { 
		return "main";
	}
	/**
	 * backend system main top
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/top"})
	public String top(HttpServletRequest httpServletRequest) { 
		return "top";
	}
	/**
	 * backend system main left
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/left"})
	public String left(HttpServletRequest httpServletRequest) { 
		return "left";
	}	
	/**
	 * backend system main content
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/content"})
	public String content(HttpServletRequest httpServletRequest) { 
		return "content";
	}
	/**
	 * backend system switch
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/swich"})
	public String swich(HttpServletRequest httpServletRequest) { 
		return "swich";
	}
	
	/**
	 * backend system bottom
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping({"/bottom"})
	public String bottom(HttpServletRequest httpServletRequest) { 
		return "bottom";
	}
	
	 @ExceptionHandler(Exception.class)
	  public ModelAndView handleError(HttpServletRequest req, Exception ex, HttpServletResponse rep) {
	    logger.error("Request: " + req.getRequestURL() + " raised " + ex);

	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", ex);
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName("error");
	    return mav;
	  }
}
