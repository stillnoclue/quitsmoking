package au.com.quitsmoking.web;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import au.com.quitsmoking.domain.SurveyAnswer;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.ISurveyAnswerManager;



@Controller
@RequestMapping({"/"})
public class StatisticsController {
	@Autowired private ISurveyAnswerManager surveyAnswerManager;

	@RequestMapping(value="/statistics", method = RequestMethod.GET)
	public String viewStats(HttpServletRequest httpServletRequest, Model model){
		User user = (User) httpServletRequest.getSession().getAttribute("loginUser");
		List<SurveyAnswer> answerList = this.surveyAnswerManager.getAnswerList(user.getUid());
		answerList=calculateCost(answerList);
		List<SurveyAnswer> last7DaysAnswerList = getLast7DaysData(answerList);
		List<SurveyAnswer> last30DaysAnswerList = getLast30DaysData(answerList);
		List<SurveyAnswer> annualAnswerList = getAnnualData(answerList);
		System.out.println(annualAnswerList.get(0).getAnswerdate());
		httpServletRequest.getSession().setAttribute("loginUser", user);
		model.addAttribute("loginUser",user);
		model.addAttribute("answers",answerList);
		model.addAttribute("weeklyAnswers",last7DaysAnswerList);
		model.addAttribute("monthlyAnswers",last30DaysAnswerList);
		model.addAttribute("annualAnswers",annualAnswerList);
		return "statistics";
	}
	
	@RequestMapping(value="/statistics", method = RequestMethod.POST)
	public String viewStats1(HttpServletRequest httpServletRequest, Model model){
		User user = (User) httpServletRequest.getSession().getAttribute("loginUser");
		List<SurveyAnswer> answerList = this.surveyAnswerManager.getAnswerList(user.getUid());
		answerList=calculateCost(answerList);
		List<SurveyAnswer> last7DaysAnswerList = getLast7DaysData(answerList);
		List<SurveyAnswer> last30DaysAnswerList = getLast30DaysData(answerList);
		List<SurveyAnswer> annualAnswerList = getAnnualData(answerList);
		System.out.println(annualAnswerList.get(0).getAnswerdate());
		httpServletRequest.getSession().setAttribute("loginUser", user);
		model.addAttribute("loginUser",user);
		model.addAttribute("answers",answerList);
		model.addAttribute("weeklyAnswers",last7DaysAnswerList);
		model.addAttribute("monthlyAnswers",last30DaysAnswerList);
		model.addAttribute("annualAnswers",annualAnswerList);
		return "statistics";
	}
	
	public List<SurveyAnswer> getLast7DaysData(List<SurveyAnswer> answerList){
		List<SurveyAnswer> last7DaysAnswerList = new ArrayList<SurveyAnswer>();
		int dateToday=getTodaysDate();
		for(int i=0; i<answerList.size();i++){
			int ad = Integer.parseInt(answerList.get(i).getAnswerdate()+"");
			if(dateToday-ad<7){
				//System.out.println("Changed Cost "+ answerList.get(i).getCost());
				last7DaysAnswerList.add(answerList.get(i));
			}
		}
		
		return last7DaysAnswerList;
	}
	
	public List<SurveyAnswer> getLast30DaysData(List<SurveyAnswer> answerList){
		System.out.println("Calling last 30 days data");
		List<SurveyAnswer> last30DaysAnswerList = new ArrayList<SurveyAnswer>();
		int dateToday=getTodaysDate();
		for(int i=0; i<answerList.size();i++){
			int ad = Integer.parseInt(answerList.get(i).getAnswerdate()+"");
			if(dateToday-ad<100){
				//System.out.println("Changed Cost "+ answerList.get(i).getCost());
				//System.out.println(answerList.get(i).getAnswerdate());
				last30DaysAnswerList.add(answerList.get(i));
			}
		}
		
		return last30DaysAnswerList;
	}
	
	public List getLast7DaysCost(List<SurveyAnswer> answerList){
		System.out.println("Calling getLast7DaysCost ");
		List costList = new ArrayList();
		for(int i=0; i<answerList.size();i++){
				System.out.println("Pack "+answerList.get(i).getPacks());
				System.out.println("Cost before "+answerList.get(i).getCost());
				costList.add(answerList.get(i).getPacks()*20);
				System.out.println("Cost "+costList.get(i));
			}
		
		
		return costList;
	}
	public int getTodaysDate(){
		int idate=0;
		String sdate="";
		 String dateInString =new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		 String[] dateParts = dateInString.split("-");
		 for (int i=dateParts.length-1; i>=0; i--){
			 sdate+=dateParts[i];
			 
		 }
		 //System.out.println("Today is "+sdate);
		 idate=Integer.parseInt(sdate);
		return idate;
	}
	public List<SurveyAnswer> calculateCost(List<SurveyAnswer> answerList){
		System.out.println("calling calculateCost");
		for (int i=0; i<answerList.size(); i++){
			answerList.get(i).setCost(answerList.get(i).getPacks()*20);
		}
		
		return answerList;
	}
	
	public List<SurveyAnswer> getAnnualData(List<SurveyAnswer> answerList){
		System.out.println("Calling get annual data");
		List<SurveyAnswer> annualList = new ArrayList<SurveyAnswer>();
		
		SurveyAnswer temp =new SurveyAnswer();
		temp.setAnswerdate("January");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("February");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("March");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("April");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("May");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("June");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("July");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("August");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("September");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("October");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("November");
		annualList.add(temp);
		temp =new SurveyAnswer();
		temp.setAnswerdate("December");
		annualList.add(temp);
		
		int[] monthlyCost = new int[12];
		int[] monthlyPack = new int[12];
		int[] monthlyCig = new int[12];
		for(int i=1; i<answerList.size();i++){
			int ad = Integer.parseInt(answerList.get(i).getAnswerdate()+"");
			int sub = ad-20160000;
			if (sub<200){
				monthlyCost[0]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[0]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[0]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<300){
				monthlyCost[1]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[1]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[1]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<400){
				monthlyCost[2]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[2]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[2]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<500){
				monthlyCost[3]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[3]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[3]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<600){
				monthlyCost[4]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[4]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[4]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<700){
				monthlyCost[5]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[5]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[5]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<800){
				monthlyCost[6]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[6]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[6]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<900){
				monthlyCost[7]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[7]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[7]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<1000){
				monthlyCost[8]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[8]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[8]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<1100){
				monthlyCost[9]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[9]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[9]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<1200){
				monthlyCost[10]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[10]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[10]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			} else if (sub<1300){
				monthlyCost[11]+=Integer.parseInt(answerList.get(i).getCost()+"");
				monthlyPack[11]+=Integer.parseInt(answerList.get(i).getPacks()+"");
				monthlyCig[11]+=Integer.parseInt(answerList.get(i).getCigarettes()+"");
			}
		}
		
		for (int i=0; i<annualList.size(); i++){
			annualList.get(i).setCost(monthlyCost[i]);
			annualList.get(i).setPacks(monthlyPack[i]);
			annualList.get(i).setCigarettes(monthlyCig[i]);
			System.out.println(annualList.get(i).getAnswerdate()+" cig "+ annualList.get(i).getCigarettes() +" packs "+ annualList.get(i).getPacks() +" cost "+ annualList.get(i).getCost());
		}
		return annualList;
	}
	
}