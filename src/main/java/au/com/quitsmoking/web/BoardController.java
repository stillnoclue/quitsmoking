package au.com.quitsmoking.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.quitsmoking.domain.Board;
import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.SurveyAnswer;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.BoardManager;
import au.com.quitsmoking.service.IBoardManager;
import au.com.quitsmoking.service.IPostManager;
import au.com.quitsmoking.service.ISurveyAnswerManager;
import au.com.quitsmoking.service.PostManager;
import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.util.AjaxResult;
import au.com.quitsmoking.util.PasswordHash;

@Controller
public class BoardController {
	
	@Autowired
	private UserService userservice;

	@Autowired
	private BoardManager boardManager;

	@Autowired
	private PostManager postManager;
	
	@RequestMapping({"/boardlist"})
	public String boardlist(HttpServletRequest request,Model model) { 
		List<Board> list=boardManager.getBoards();
		model.addAttribute("boardlist",list);
		return "boardlist";
	}
	
	@RequestMapping(value="/addboard")
	@ResponseBody
	public AjaxResult addboard (HttpServletRequest request, 
								@ModelAttribute("board") Board board, 
								Model model) {
		AjaxResult ajaxresult = new AjaxResult();
		ajaxresult.setUrl("");
		try {
			List dbList = boardManager.getBoardByBoardName(board.getBoardName());
			if (dbList.size() > 0) {
				throw new Exception("The board name exits!");
			}
			board.setBoardName(board.getBoardName());
			board.setLastPostDate("1970-01-01 00:00:00.000");
			board.setLastPostId(0);
			board.setPostCount(0);
			this.boardManager.addBoard(board);
			
			ajaxresult.setSuccess(true);
			ajaxresult.setMsg("Success!");
		} catch (Exception e) {
			ajaxresult.setSuccess(false);
			ajaxresult.setMsg("Fail to save: " + e.getMessage());
		}
		return ajaxresult;
	}

 	@RequestMapping(value="/delboard/{boardid}")
 	@ResponseBody
 	public AjaxResult editboard (HttpServletRequest request, 
 								@PathVariable("boardid") int bid, 
 								Model model) {
 		AjaxResult ajaxresult = new AjaxResult();
 		ajaxresult.setUrl("/boardlist");
 		try {
 			Board board = boardManager.getBoardByBoardId(bid);
 			
 			if (!postManager.getPostsByBoardId(bid).isEmpty()) {
 				throw new Exception("You are not allowed to remove a board with posts inside.");
 			}
 			boardManager.delBoard(board);
 			ajaxresult.setSuccess(true);
 			ajaxresult.setMsg("Success!");
 		} catch (Exception e) {
 			ajaxresult.setSuccess(false);
 			ajaxresult.setMsg("Fail to save: " + e.getMessage());
 		}
 		return ajaxresult;
 	}
}
