package au.com.quitsmoking.web;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;


import au.com.quitsmoking.domain.Board;
import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.Reply;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.IBoardManager;
import au.com.quitsmoking.service.IPostManager;
import au.com.quitsmoking.service.IReplyManager;

import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.util.JsonReader;

@Controller
public class ForumController {
	@Autowired
	IBoardManager boardManager;
	@Autowired
	IPostManager postManager;
	@Autowired
	IReplyManager replyManager;
	@Autowired
	private UserService userservice;
	
	@RequestMapping(value = "/forum")
	public String showBoard(Model theModel){
		
		List<Board> boards = boardManager.getBoards();
		// set the map for displaying reply count.
		Map<Integer, Integer> postMap = new HashMap<Integer, Integer>();
		Map<Integer, Post> latestPost = new HashMap<Integer, Post>();
		
		// create a hashmap for uid-username mapping in the view.
		Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();

		
		
		for (Board b : boards) {
			postMap.put(b.getBoardId(), postManager.getNumberOfPostsByPostId(b.getBoardId()));
			
			//get the latest post map for different boards  
			latestPost.put(b.getBoardId(), postManager.getLatestPostByBoardId(b.getBoardId()));
			
			uidUsernameMap.put(postManager.getLatestPostByBoardId(b.getBoardId()).getAuthor(), userservice.getUserByUID(postManager.getLatestPostByBoardId(b.getBoardId()).getAuthor()).getUsername()  );
		}
		
		theModel.addAttribute("boards",boards);
		theModel.addAttribute("postMap",postMap);
		theModel.addAttribute("latestPost",latestPost);
		theModel.addAttribute("uidUsernameMap",uidUsernameMap);
		return "board";
	}
	@RequestMapping(value = "/post")
	public String showAPost(Model theModel,
			@RequestParam(value="postId") String postId,
			@RequestParam(value="replyToReplyId") String replyToReplyId){
		
		// get post from the manager
		int Id = Integer.parseInt(postId);
		Post thePost = postManager.getPostById(Id);
		
		// add the post to the model
		theModel.addAttribute("post",thePost);

		// get the list of replies from the manager
		List<Reply> replies = replyManager.getReplies(Id);
		
		// add replies to the model
		theModel.addAttribute("replies",replies);
		
		// create a hashmap for uid-username mapping in the view.
		Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();

		// add the post user name map
		uidUsernameMap.put(thePost.getAuthor(), userservice.getUserByUID(thePost.getAuthor()).getUsername());
		
		// cycle through replies to map user ID and username
		for (Reply reply : replies){
			uidUsernameMap.put(reply.getAuthor(),userservice.getUserByUID(reply.getAuthor()).getUsername());
		}
		
		if (replyToReplyId!=""){
			Reply replyToReply = replyManager.getReplyByReplyId(Integer.parseInt(replyToReplyId));
			theModel.addAttribute("replyToReply",replyToReply);
			theModel.addAttribute("replyToReplyId",replyToReply.getReplyId());
		}else
		{
			theModel.addAttribute("replyToReplyId","");
			
		}
		
		theModel.addAttribute("uidUsernameMap",uidUsernameMap);
		
		return "post";
	}

	@RequestMapping(value = "/list")
	public String showPostList(Model theModel,
			@RequestParam("boardId") String boardId){
		List<Post> posts = postManager.getPostsByBoardId(Integer.parseInt(boardId));
		String boardName = boardManager.getBoardNameByboardId(Integer.parseInt(boardId));
		// set the map for displaying reply count.
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		// create a hashmap for uid-username mapping in the view.
		Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();
		
		for (Post p : posts) {
			  map.put(p.getPostId(), postManager.getNumberOfRepliesByPostId(p.getPostId()));
			  uidUsernameMap.put(p.getAuthor(), userservice.getUserByUID(p.getAuthor()).getUsername());
		}

		theModel.addAttribute("posts",posts);
		theModel.addAttribute("boardId",boardId);
		theModel.addAttribute("boardName", boardName);
		theModel.addAttribute("replyMap",map);
		theModel.addAttribute("uidUsernameMap",uidUsernameMap);
		
		return "list";
	}
	@RequestMapping(value="/newPost")
	public String newPost(Model theModel,
			HttpServletRequest httpServletRequest,
			@RequestParam("topic") String topic,
			@RequestParam("content") String content,
			@RequestParam("boardId") String boardId){
		//check if login user, if not return login.
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		if (loginUser == null){
			return "login";
		}else{
			long userId = loginUser.getUid(); 
			Post newPost = postManager.newPost(content, (int) userId, topic, Integer.parseInt(boardId));
			
			theModel.addAttribute("post",newPost);
			
			return "post";
		}

	}
	
	@RequestMapping(value="/newReply")
	public String newReply(Model theModel,
			HttpServletRequest httpServletRequest,
			@RequestParam("content") String content,
			@RequestParam("postId") String postId,
			@RequestParam("g-recaptcha-response") String captcha) throws JSONException, IOException{
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		if (loginUser == null){
			return "login";
		}else{
			
			
			if (captcha == null){
				return "captcha-error";
			}else{
				String secretKey ="6LdvMAoUAAAAAH7Pr7exsv8z-mMM0yGw5H9IfL3e";
				String ip = httpServletRequest.getRemoteAddr();

				if ((Boolean) JsonReader.readJsonFromUrl("https://www.google.com/recaptcha/api/siteverify?secret="+secretKey+"&response="+captcha+"&remoteip="+ip).get("success")){
					// captcha is verified
					long userId = loginUser.getUid(); 
					Reply newReply = replyManager.newReply((int) userId, content, Integer.parseInt(postId)); 
					
					theModel.addAttribute("post",postManager.getPostById(Integer.parseInt(postId)));
					
					return "redirect";
				}else{
					long userId = loginUser.getUid(); 
					//Reply newReply = replyManager.newReply((int) userId, content, Integer.parseInt(postId)); 

					theModel.addAttribute("post",postManager.getPostById(Integer.parseInt(postId)));
					
					return "redirect";
				}
			}
			
			
			
			
			

		}

	}
	
	@RequestMapping(value="/captchatest")
	public String captchatTest(Model theModel){
		return "captchatest";
	}
	
	

	/*@RequestMapping(value="/newBoard")
	public String newBoard(Model theModel,
			HttpServletRequest httpServletRequest,
			@RequestParam("boardName") String boardName){
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		if (loginUser == null){
			return "login";
		}else{
			if (loginUser.getRole() != 3){ // only admin can create new boards.
				return "login";
			}else{
				boardManager.addBoard(boardName);
				return "forumlist";
			}

		}
	}
	
	@RequestMapping(value="/modifyBoardName")
	public String modifyBoardName(Model theModel,
			HttpServletRequest httpServletRequest,
			@RequestParam("boardId") String boardId,
			@RequestParam("boardName") String boardName
			){
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		if (loginUser == null){
			return "login";
		}else{
			if (loginUser.getRole() != 3){ // only admin can modify board names
				return "login";
			}else{
				boardManager.modifyBoardName(Integer.parseInt(boardId),boardName);
				boardManager.getBoards();
				return "forumlist";
			}

		}
	}*/
	
}
