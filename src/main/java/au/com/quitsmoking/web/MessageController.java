package au.com.quitsmoking.web;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.quitsmoking.domain.Board;
import au.com.quitsmoking.domain.Message;
import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.IBoardManager;
import au.com.quitsmoking.service.IMessageManager;
import au.com.quitsmoking.service.IPostManager;
import au.com.quitsmoking.service.IReplyManager;
import au.com.quitsmoking.service.UserService;

@Controller
public class MessageController {
	@Autowired
	IBoardManager boardManager;
	@Autowired
	IPostManager postManager;
	@Autowired
	IReplyManager replyManager;
	@Autowired
	IMessageManager messageManager;
	@Autowired
	UserService userservice;
	
	
	
	@RequestMapping(value = "/message-centre")
	public String showMessageCentre(HttpServletRequest httpServletRequest, Model theModel){

		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		
		if(loginUser == null){
			return "login";
		}else{
			int userId = (int) loginUser.getUid();
			int inboxCount = messageManager.getInboxCount(userId);
			int outboxCount = messageManager.getOutboxCount(userId);
			
			theModel.addAttribute("inboxCount",inboxCount);
			theModel.addAttribute("outboxCount",inboxCount);
			return "message-centre";
		}
		
		
		

	}

	
	@RequestMapping(value = "/inbox")
	public String showInbox(HttpServletRequest httpServletRequest, Model theModel){
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		
		// create a hashmap for uid-username mapping in the view.
		Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();
		
		if(loginUser == null){
			return "login";
		}else{
			int userId = (int) loginUser.getUid();
			int inboxCount = messageManager.getInboxCount(userId);
			List<Message> messages =  messageManager.getMessagesToUser(userId);
			for(Message theMessage:messages){
				uidUsernameMap.put(theMessage.getFromUserId(), userservice.getUserByUID(theMessage.getFromUserId()).getUsername());	
			}
			theModel.addAttribute("messages",messages);
			theModel.addAttribute("uidUsernameMap",uidUsernameMap);
			theModel.addAttribute("inboxCount",inboxCount);
			return "inbox";
		}
		
		
		

	}
	
	@RequestMapping(value = "/outbox")
	public String showOutbox(HttpServletRequest httpServletRequest, Model theModel){
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		
		if(loginUser == null){
			return "login";
		}else{
			int userId = (int) loginUser.getUid();
			List<Message> messages =  messageManager.getMessagesFromUser(userId);
			
			theModel.addAttribute("messages",messages);
			
			return "outbox";
		}
		
		
		

	}

	@RequestMapping(value= "/composeMessage")
	public String composeMessage(HttpServletRequest httpServletRequest, Model theModel){

			return "composeMessage";

		
		
		
		
	}
	
	@RequestMapping(value = "/sendMessage")
	public String sendMessage(HttpServletRequest httpServletRequest, Model theModel,
			@RequestParam("toUserId") String strToUserId,
			@RequestParam("subject") String subject,
			@RequestParam("content") String content
			){
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		
		if(loginUser == null){
			return "login";
		}else{
			int userId = (int) loginUser.getUid();
			LocalTime time = LocalTime.now();
			Message theMessage = messageManager.createMessage(userId, Integer.parseInt(strToUserId), subject, content);
			theModel.addAttribute("message",theMessage);
			theModel.addAttribute("place","outbox");
			return "message";
		}
		

	}
	
	@RequestMapping(value = "/showMessage")
	public String showMessage(HttpServletRequest httpServletRequest, Model theModel,
			@RequestParam("messageId") String strMessageId,
			@RequestParam("place") String place
			){
		
		User loginUser = (User) httpServletRequest.getSession().getAttribute("loginUser");
		
		if(loginUser == null){
			return "login";
		}else{
			int userId = (int) loginUser.getUid();
			
			// create a hashmap for uid-username mapping in the view.
			Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();
			
			Message theMessage = messageManager.getMessageByMessageId(Integer.parseInt(strMessageId));
			messageManager.markAsRead(theMessage.getMessageId());
			
			
			uidUsernameMap.put(theMessage.getFromUserId(), userservice.getUserByUID(theMessage.getFromUserId()).getUsername());
			theModel.addAttribute("message",theMessage);
			theModel.addAttribute("place",place);
			theModel.addAttribute("uidUsernameMap",uidUsernameMap);
					
			return "message";
		}

		
	}
	
}
	