/*package au.com.quitsmoking.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.quitsmoking.domain.Post;
import au.com.quitsmoking.domain.SurveyAnswer;
import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.IPostManager;
import au.com.quitsmoking.service.ISurveyAnswerManager;
import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.util.PasswordHash;

@Controller
public class LoginController {
	
	@Autowired
	private UserService userservice;
	@Autowired
	private ISurveyAnswerManager surveyAnswerManager;
	@Autowired
	private IPostManager postManager;

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest httpServletRequest, Model model, HttpServletResponse response) {
		httpServletRequest.getSession().removeAttribute("loginUser");
		httpServletRequest.getSession().invalidate();
		return "logout";
	}
	
	@RequestMapping(value = "/adminlogout", method = RequestMethod.GET)
	public String adminlogout(HttpServletRequest httpServletRequest, Model model, HttpServletResponse response) {
		httpServletRequest.getSession().removeAttribute("loginUser");
		httpServletRequest.getSession().invalidate();
		return "login";
	}

	@RequestMapping({ "/" })
	public String login(HttpServletRequest httpServletRequest, Model model) {
		return "login";
	}
	
	@RequestMapping({"/adminlogin"})
	public String adminlogin(HttpServletRequest httpServletRequest) { 
		return "adminlogin";
	}
	
	@RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute User user, Model model, HttpServletRequest request){
		String passwordHash = DigestUtils.md5Hex(user.getPassword());
		System.out.println("-------------------------------------------");
		System.out.println(passwordHash);
		System.out.println("-------------------------------------------");
		User loginUser = this.userservice.getUserByUsername(user.getUsername());
		boolean valid;
		try {
			valid = PasswordHash.validatePassword(user.getPassword(), loginUser.getPassword());
		} catch (Exception e) {
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Invalid authentication credentials");
			return "login";
		}
		if (!valid) {
			model.addAttribute("msgTitle", "Error");
			model.addAttribute("msgContent", "Invalid authentication credentials");
			return "login";
		}
		user = this.userservice.getUserByUsername(user.getUsername());
		request.getSession().setAttribute("loginUser", user);
		if (user.getRole() == 1) {
			if (user.getStatus() == 1) {
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "Member Account is locked!");
				return "login";
			} else if (user.getStatus() == 0) {
				SurveyAnswer todayAnswer = this.surveyAnswerManager.getTodayAnswer(user.getUid());

				// create a hashmap for uid-username mapping in the view.
				Map<Integer, String> uidUsernameMap = new HashMap<Integer, String>();
				
				List<Post> posts = this.postManager.getTenLatestPosts();
				for(Post thePost : posts){
					// add the post user name map
					uidUsernameMap.put(thePost.getAuthor(), userservice.getUserByUID(thePost.getAuthor()).getUsername());	
				}

				

				if (todayAnswer == null)
					todayAnswer = new SurveyAnswer();
				model.addAttribute("loginUser", user);
				model.addAttribute("answer", todayAnswer);
				model.addAttribute("posts", posts);
				model.addAttribute("uidUsernameMap",uidUsernameMap);
				return "home";
			}
			
		} else if (user.getRole() == 2) {
			if (user.getStatus() == 0) {
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "GP License is in process of verification! Please return soon.");
				return "login";
			} else if (user.getStatus() == 1) {
				List<User> patientList = this.userservice.getPatientList(user.getUid());
				model.addAttribute("loginUser", user);
				model.addAttribute("patientList",patientList);
				return "gphome";
			} else if (user.getStatus() == 2) {
				model.addAttribute("msgTitle", "Error");
				model.addAttribute("msgContent", "GP License is not approved.");
				return "login";
			}
			
		} else if (user.getRole() == 3) {
			model.addAttribute("loginUser", user);
			return "main";
		}
		return "login";
	}

}
*/