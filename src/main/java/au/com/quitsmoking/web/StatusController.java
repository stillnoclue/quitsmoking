package au.com.quitsmoking.web;

import javax.servlet.http.HttpServletRequest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.quitsmoking.domain.User;
import au.com.quitsmoking.service.ISurveyAnswerManager;
import au.com.quitsmoking.service.UserService;
import au.com.quitsmoking.domain.SurveyAnswer;

@Controller
public class StatusController {
	private static final String List = null;
	@Autowired
	private UserService userservice;
	@Autowired
	private ISurveyAnswerManager surveyAnswerManager;
	
	@RequestMapping("/status")
	public String status(Model model, HttpServletRequest request){
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		List<SurveyAnswer> l = this.surveyAnswerManager.getAnswerList(loginUser.getUid());
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String today = df.format(date);
		Integer startdate = Integer.parseInt(today);
		List<Long> status = getStatus(loginUser);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("startdate",startdate);
		model.addAttribute("status",status);
		return "status";
	}
	
	public List<Long> getStatus( User u){
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String today = df.format(date);
		Integer startdate = Integer.parseInt(today);
		List<Long> list = new ArrayList();
		for(int i = 1;i <= 7; i++){
			startdate = startdate - (i + 1);
			Integer endate = startdate - 1;
			SurveyAnswer thisday = this.surveyAnswerManager.getRecordForThisUserAndThisDate(u.getUid(), startdate.toString());
			SurveyAnswer yesterday = this.surveyAnswerManager.getRecordForThisUserAndThisDate(u.getUid(),endate.toString());
			Long cig1 = 0l;
			Long cig2 = 0l;
			Long diff = 0l;
			if(thisday != null){
				cig1 = thisday.getCigarettes();
			}
			if(yesterday != null){
				cig2 = yesterday.getCigarettes();
			}
			diff = cig1 - cig2;
			//System.out.println("cig 1 = " + cig1);
			//System.out.println("cig 2 = " + cig2);
			//System.out.println("diff = " + diff);
			
			list.add(diff);
		}
		return list;
		
	}
}
