package au.com.quitsmoking.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="reply")
public class Reply implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="reply_id")
	private int replyId;
	
	@Column(name="author")
	private int author;
	
	@Column(name="content")
	private String content;

	@Column(name="time")
	private String time;
	
	@Column(name="post_id")
	private int postId;
	
	

	public Reply(){
		
	}


	public Reply(int author, String content, String time, int postId) {
		super();
		this.author = author;
		this.content = content;
		this.time = time;
		this.postId = postId;
	}



	public int getReplyId() {
		return replyId;
	}



	public void setReplyId(int replyId) {
		this.replyId = replyId;
	}



	public int getAuthor() {
		return author;
	}



	public void setAuthor(int author) {
		this.author = author;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	public String getTime() {
		return time;
	}



	public void setTime(String time) {
		this.time = time;
	}



	public int getPostId() {
		return postId;
	}



	public void setPostId(int post_id) {
		this.postId = post_id;
	}





	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Reply [replyId=" + replyId + ", author=" + author + ", content=" + content + ", time=" + time
				+ ", postId=" + postId + "]";
	}

	
	public String getDisplayTime(){

		return time.substring(0,10)+" "+time.substring(11,16);
	}

}
