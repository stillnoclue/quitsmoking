package au.com.quitsmoking.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="message")
public class Message implements Serializable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="message_id")
	private int messageId;
	
	@Column(name="from_user_id")
	private int fromUserId;
	
	@Column(name="to_user_id")
	private int toUserId;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="content")
	private String content;
	
	@Column(name="if_read")
	private int ifRead;
	
	@Column(name="post_id")
	private int postId;
	
	@Column(name="reply_id")
	private int replyId;
	
	@Column(name="time")
	private String time;
	
	@Column(name="reply_message_id")
	private int replyMessageId;
	
	public Message(){
		
	}

	public Message(int fromUserId, int toUserId, String subject, String content, String time) {
		super();
		this.fromUserId = fromUserId;
		this.toUserId = toUserId;
		this.subject = subject;
		this.content = content;
		this.ifRead = 0;
		this.time = time;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getFrom() {
		return fromUserId;
	}

	public void setFrom(int from) {
		this.fromUserId = from;
	}

	public int getToUserId() {
		return toUserId;
	}

	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getIfRead() {
		return ifRead;
	}

	public void setRead(int ifRead) {
		this.ifRead = ifRead;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getReplyId() {
		return replyId;
	}

	public void setReplyId(int replyId) {
		this.replyId = replyId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getReplyMessageId() {
		return replyMessageId;
	}

	public void setReplyMessageId(int replyMessageId) {
		this.replyMessageId = replyMessageId;
	}

	public int getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(int fromUserId) {
		this.fromUserId = fromUserId;
	}

	public void setIfRead(int ifRead) {
		this.ifRead = ifRead;
	}


//helper methods
	
	public String getReadStatus(){
		if (this.ifRead == 1){
			return "Read";
		}else{
			return "Unread";
			
		}
	}
	public String getDisplayTime(){

		return time.substring(0,10)+" "+time.substring(11,16);
	}

	
	
}
