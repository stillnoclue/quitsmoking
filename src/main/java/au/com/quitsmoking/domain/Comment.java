package au.com.quitsmoking.domain;

import java.sql.Date;

public class Comment {
	private long id;
	private String content;
	private String commented_date;
	private long post_id;
	private long user_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCommented_date() {
		return commented_date;
	}

	public void setCommented_date(String commented_date) {
		this.commented_date = commented_date;
	}

	public long getPost_id() {
		return post_id;
	}

	public void setPost_id(long post_id) {
		this.post_id = post_id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

}
