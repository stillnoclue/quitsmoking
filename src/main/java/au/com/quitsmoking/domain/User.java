package au.com.quitsmoking.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "User")
public class User implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "Uid")
	private long uid;

	@Column(name = "Username")
	private String username;

	@Column(name = "Firstname")
	private String firstname;

	@Column(name = "Lastname")
	private String lastname;

	@Column(name = "Password")
	private String password;

	@Column(name = "Email")
	private String email;

	@Column(name = "Role")
	private int role;

	@Column(name = "Phonenum")
	private String phonenum;

	@Column(name = "Date_of_birth")
	private String date_of_birth;

	@Column(name = "Created_date")
	private String created_date;

	@Column(name = "Gp_license")
	private String gp_license;

	@Column(name = "Status")
	private int status;

	@Column(name = "Receive_email")
	private int receive_email;

	@Column(name = "Aboutme")
	private String aboutme;

	@Column(name = "gpid")
	private long gpid=0;

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getGp_license() {
		return gp_license;
	}

	public void setGp_license(String gp_license) {
		this.gp_license = gp_license;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getReceive_email() {
		return receive_email;
	}

	public void setReceive_email(int receive_email) {
		this.receive_email = receive_email;
	}

	public String getAboutme() {
		return aboutme;
	}

	public void setAboutme(String aboutme) {
		this.aboutme = aboutme;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Username: " + username + ";");
		buffer.append("Firstname: " + firstname + ";");
		buffer.append("Lastname: " + lastname + ";");
		buffer.append("Password: " + password + ";");
		buffer.append("Email: " + email + ";");
		buffer.append("Role: " + role + ";");
		buffer.append("Phonenum: " + phonenum + ";");
		buffer.append("Date_of_birth: " + date_of_birth + ";");
		buffer.append("Created_date: " + created_date + ";");
		buffer.append("Gp_license: " + gp_license + ";");
		buffer.append("Status: " + status + ";");
		buffer.append("Receive_email: " + receive_email + ";");
		buffer.append("Aboutme: " + aboutme);
		return buffer.toString();
	}

	public long getGpid() {
		return gpid;
	}

	public void setGpid(long gpid) {
		this.gpid = gpid;
	}
}
