package au.com.quitsmoking.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "surveyanswer")
public class SurveyAnswer implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "AID")
	private long id;
	@Column(name = "User_UID")
	private long user_id;
	@Column(name = "cigarettesmoked")
	private long cigarettes;
	@Column(name = "packbought")
	private long packs;
	@Column(name = "answerdate")
	private String answerdate = "TBD";
	@Column(name = "cost")
	private long cost=0;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getCigarettes() {
		return cigarettes;
	}

	public void setCigarettes(long cigarettes) {
		this.cigarettes = cigarettes;
	}

	public long getPacks() {
		return packs;
	}

	public void setPacks(long packs) {
		this.packs = packs;
	}

	public String getAnswerdate() {
		return answerdate;
	}

	public void setAnswerdate(String answerdate) {
		this.answerdate = answerdate;
	}

	public long getCost() {
		return cost;
	}

	public void setCost(long cost) {
		this.cost = cost;
	}

}
