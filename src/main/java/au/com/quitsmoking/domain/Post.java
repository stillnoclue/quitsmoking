package au.com.quitsmoking.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="post")
public class Post implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="post_id")
	private int postId;
	
	@Column(name="content")
	private String content;
	
	@Column(name="time")
	private String time;

	@Column(name="author")
	private int author;
	
	@Column(name="topic")
	private String topic;
	
	@Column(name="board_id")
	private int boardId;
	
	

	public Post(){
		
	}

	public Post(String content, String time, int author, String topic, int boardId) {
		super();
		this.content = content;
		this.time = time;
		this.author = author;
		this.topic = topic;
		this.boardId = boardId;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getAuthor() {
		return author;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getBoardId() {
		return boardId;
	}

	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}

	@Override
	public String toString() {
		return "Post [postId=" + postId + ", content=" + content + ", time=" + time + ", author=" + author + ", topic="
				+ topic + ", boardId=" + boardId + "]";
	}
	public String getDisplayTime(){

		return time.substring(0,10)+" "+time.substring(11,16);
	}
}
