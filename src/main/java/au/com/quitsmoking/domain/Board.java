package au.com.quitsmoking.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="board")
public class Board implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="board_id")
	private int boardId;
	
	@Column(name="board_name")
	private String boardName;
	
	@Column(name="post_count")
	private int postCount;

	@Column(name="last_post_date")
	private String lastPostDate;
	
	@Column(name="last_post_id")
	private int lastPostId;

	public Board(){
		
	}
	public Board(String boardName) {
		super();
		this.boardName = boardName;
		this.postCount = 0;
	}

	
	public int getBoardId() {
		return boardId;
	}
	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}
	public String getBoardName() {
		return boardName;
	}
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}
	public int getPostCount() {
		return postCount;
	}
	public void setPostCount(int postCount) {
		this.postCount = postCount;
	}
	public String getLastPostDate() {
		return lastPostDate;
	}
	public void setLastPostDate(String lastPostDate) {
		this.lastPostDate = lastPostDate;
	}
	public int getLastPostId() {
		return lastPostId;
	}
	public void setLastPostId(int lastPostId) {
		this.lastPostId = lastPostId;
	}

	
	@Override
	public String toString() {
		return "Board [boardId=" + boardId + ", boardName=" + boardName + ", postCount=" + postCount + ", lastPostDate="
				+ lastPostDate + ", lastPostId=" + lastPostId + "]";
	}

} 
