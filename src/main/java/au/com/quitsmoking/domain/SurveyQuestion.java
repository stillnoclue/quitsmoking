package au.com.quitsmoking.domain;

public class SurveyQuestion {
	private long id;
	private String Question;
	private String AnswerType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return Question;
	}

	public void setQuestion(String question) {
		Question = question;
	}

	public String getAnswerType() {
		return AnswerType;
	}

	public void setAnswerType(String answerType) {
		AnswerType = answerType;
	}

}
