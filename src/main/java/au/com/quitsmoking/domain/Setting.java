package au.com.quitsmoking.domain;

public class Setting {
	private long id;
	private String Setting1;
	private long user_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSetting1() {
		return Setting1;
	}

	public void setSetting1(String setting1) {
		Setting1 = setting1;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
}
