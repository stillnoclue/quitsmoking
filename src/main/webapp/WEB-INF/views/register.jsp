
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>User Register</title>
	<link href="css/dhtmlx_pro_full/dhtmlx.css" rel="stylesheet"
		type="text/css"></link>
	<link href="css/jquery.qtip.min.css" rel="stylesheet" type="text/css"></link>
	<script src="css/dhtmlx_pro_full/dhtmlx.js"></script>
	<script src="js/jquery-1.10.1.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/messages_en.js"></script>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/jquery.qtip.min.js"></script>

	<script language="javascript">
		
		var dialogwins;
		
		window.onload=function(){
			dialogwins= new dhtmlXWindows();
		}
		</script>
	<script language="javascript">
		
		$(function(){
            
			$("#btnAdd").click(function(){
              $("#user").validate();				
				if(!$("#user").valid()){								
					return false;
				}
				
				$("#user").ajaxSubmit({
					url:"saveregister",
					type:'POST',
					dataType:'json',
					success:function(data){
						dhtmlx.alert({title:"Attention",text:data.msg,ok:"OK",callback: function() { 
							
						}});
						}
					});
				return false;
			});	 
			
		});
	</script>
	<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
			<meta name="author" content="">

				<title>Register</title> <!-- Bootstrap core CSS -->
				<link href="<c:url value="/resources/css/bootstrap.min.css" />"
					rel="stylesheet">

					<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
					<link
						href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
						rel="stylesheet">

						<!-- Custom styles for this template -->
						<link href="<c:url value="/resources/css/dashboard.css" />"
							rel="stylesheet">


							<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
							<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

	<body>

		<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="/"><img src="images/main/logo_black.png" width="280px" alt="" /></a>

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse"></div>
		</div>
		</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
						
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

					<form id="user" name="user" action="/addUser" method="post">
						<h2 class="form-signin-heading">Welcome to register!</h2>
						<table style="margin: 20px">
							<tr>
								<td>Username:</td>
								<td><label for="inputUserName" class="sr-only">User
										Name</label> <input type="text" id="username" name="username"
									pattern="^[a-z0-9_-]{3,15}$"
									title="Username must between 3 and 15 characters. Can only contain lowercase letters,numbers and underscore(-)"
									class="form-control" placeholder="User name" value="" required
									autofocus></td>
							</tr>
							<tr>
								<td>First Name:</td>
								<td><label for="inputFirstName" class="sr-only">First
										Name</label> <input type="text" id="firstname" name="firstname"
									pattern="^[a-zA-Z0-9 ]*$"
									title="Can only contain letters numbers and space( )"
									class="form-control" placeholder="First name" value="" required
									autofocus></td>
							</tr>
							<tr>
								<td>Last Name:</td>
								<td><label for="inputLastName" class="sr-only">Last
										Name</label> <input type="text" id="lastname" name="lastname"
									pattern="^[a-zA-Z0-9 ]*$"
									title="Can only contain characters, numbers and space( )"
									class="form-control" placeholder="Last name" value="" required
									autofocus></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><label for="inputPassword" class="sr-only">Password</label>
									<input type="password" id="password" name="password"
									pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
									title="Password must be at least 8 characters long. Must contain at least one uppercase letter, one lowercase letter and one number."
									class="form-control" placeholder="Password" value="" required
									autofocus></td>
							</tr>
							<tr>
								<td>E-Mail:</td>
								<td><label for="inputEmail" class="sr-only">E-Mail</label>
									<input type="text" id="email" name="email" class="form-control"
									pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
									title="Please input valid email format. Example. abc@nodmain.com"
									placeholder="E-Mail" value="" required autofocus></td>
							</tr>
							<tr>
								<td>Phone:</td>
								<td><label for="inputPhone" class="sr-only">Phone</label> <input
									type="text" id="phonenum" name="phonenum" class="form-control"
									pattern="[0-9]+" title="Must contain numbers only."
									placeholder="Phone" value="" required autofocus></td>
							</tr>
							<tr>
								<td>Date of Birth:</td>
								<td><label for="inputDOB" class="sr-only">Date of
										Birth</label> <input type="text" id="date_of_birth"
									name="date_of_birth" class="form-control"
									placeholder="Date of Birth" value="" required autofocus
									readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></td>
							</tr>
							<tr>
								<td>Role:</td>
								<td><label for="inputRole" class="sr-only">Role</label> <select
									id="role" name="role" class="form-control" required autofocus>
										<option value="1">Member</option>
										<option value="2">General Practitioners</option>
																		<c:if test="${loginUser != null}">
									<c:if test="${loginUser.role == 3}">
										<option value="3">Administrator</option>
									</c:if>
								</c:if>
								</select></td>
							</tr>
							<tr>
								<td>GP License:</td>
								<td><label for="inputgplicense" class="sr-only">GP
										License</label> <input type="text" id="gp_license" name="gp_license"
									class="form-control" placeholder="GP License" value=""></td>
							</tr>
							<tr>
								<td><input name="btnAdd" type="submit" id="btnAdd" class="btn btn-primary"
									value="Submit" /></td>
								<td><input name="btnReset" type="reset" id="btnReset" class="btn btn-primary"
									value="Reset" /></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
		<c:if test="${ msgTitle != null}">
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">${msgTitle}</h4>
					</div>
					<div class="modal-body">
						<p>${msgContent}</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		</c:if>
		<!-- Bootstrap core JavaScript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
		<script src="<c:url value="/resources/js/holder.min.js" />"></script>
		<script
			src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"></script>
		<!-- Showing Message on Error or Success
	==================================================  -->
		<c:if test="${ msgTitle != null}">
			<script type="text/javascript">
		    $(window).load(function(){
		        $('#myModal').modal('show');
		    });
			</script>
		</c:if>
		<!-- Filling form back for user on Error
	==================================================  -->
		<c:if test="${user != null }">
			<script>
		$('input[name=username]').val('${user.username}');
		$('input[name=firstname]').val('${user.firstname}');
		$('input[name=lastname]').val('${user.lastname}');
		$('input[name=email]').val('${user.email}');
		$('input[name=gp_license]').val('${user.gp_license}');
		$('input[name=password]').val('${user.password}');
		$('input[name=phonenum]').val('${user.phonenum}');
		$('input[name=date_of_birth]').val('${user.date_of_birth}');
	</script>
			<c:choose>
				<c:when test="${user.role == 1}">
					<script>$('#role option[value="1"]').attr('selected', 'selected')</script>
				</c:when>
				<c:when test="${user.role == 2}">
					<script>$('#role option[value="2"]').attr('selected', 'selected')</script>
				</c:when>
			</c:choose>
		</c:if>
	</body>
</html>