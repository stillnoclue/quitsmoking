<%@ include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Status</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/dashboard.css" />"
	rel="stylesheet">

<script
	src="${pageContext.request.contextPath}/resources/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img src="images/main/logo_black.png" width="280px" alt="" />
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<form action="/search" method="POST" style="margin-top: 33px;">
					<ul class="nav navbar-nav navbar-right">
						<li><input type="text" name="keyword" value=""
							placeholder="Search members" /></li>
						<li><input type="image"
							src="/resources/images/glyphicons-28-search.png" alt="search"
							style="background-color: white; height: 26px" /></li>
					</ul>
				</form>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
						
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h4>Current Status of cigarettes consumption</h4>
				<table class="table-striped" style="width: 100%; font-size: 12pt;">
					<c:forEach var="i" begin="1" end="7">
						<c:choose>
							<c:when test="${status.get(i-1) > 0}">
								<tr>
									<td>From ${startdate - (i + 1) } to ${startdate - i }, number of cigarettes increased by ${status.get(i-1) }</td>
								</tr>
							</c:when>
							<c:when test="${status.get(i-1) < 0}">
								<tr>
									<td>From ${startdate - (i + 1) } to ${startdate - i }, number of cigarettes decreased by ${Math.abs(status.get(i-1)) }</td>
								</tr>
							</c:when>
							<c:when test="${status.get(i-1) == 0}">
								<tr>
									<td>From ${startdate - (i + 1) } to ${startdate - i }, number of cigarettes smoked is the same.</td>
								</tr>
							</c:when>
						</c:choose>
					</c:forEach>
				</table>
			</div>
		</div>


	</div>




	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="${pageContext.request.contextPath}/resources/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script
		src="${pageContext.request.contextPath}/resources/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
	<script
		src="${pageContext.request.contextPath}/resources/holder.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="${pageContext.request.contextPath}/resources/ie10-viewport-bug-workaround.js"></script>


</body>
</html>

