
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page session="false"%>
<html>
<head>
<title>User Log-In</title>
<link href="css/alogin.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form id="form1" action="/verifylogin" method="post">
		<div class="Main">
			<ul>
				<li class="top"></li>
				<li class="top2"></li>
				<li class="topA"></li>
				<li class="topB"><span><img src="images/login/logo.png"
						width="280px" alt="" style="margin-top: 60px" /></span></li>
				<li class="topC"></li>
				<li class="topD">
					<ul class="login">
						<li><span class="left login-text"
							style="font-weight: bold; font-size: 13px">Username：</span> <span
							style=""> <input id="Text1" type="text" class="txt"
								name="username" style="margin-left: 25px" />
						</span></li>
						<li><span class="left login-text"
							style="font-weight: bold; font-size: 13px">Password：</span> <span
							style=""> <input id="Text2" type="password" class="txt"
								name="password" style="margin-left: 25px" />
						</span></li>
					</ul>
				</li>
				<li class="topE"></li>
				<li class="middle_A"></li>
				<li class="middle_B"></li>
				<li class="middle_C"><span class="btn"><a href="login">
							<input name="login" type="image"
							src="images/login/login_button.png" />
					</a></span></li>
				<li class="middle_D"></li>
				<li class="bottom_A"></li>
				<li class="bottom_B">Don't have an account? <a href="/register">Register</a>
					Today!
				</li>
			</ul>
		</div>
	</form>
	<%-- 	<c:if test="${ msgTitle != null}">
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">${msgTitle}</h4>
					</div>
					<div class="modal-body">
						<p>${msgContent}</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</c:if> --%>
			<!-- Showing Message on Error or Success
	==================================================  -->
		<c:if test="${ msgTitle != null}">
			<script type="text/javascript">
				alert("${msgContent}");
			</script>
		</c:if>
</body>

</html>