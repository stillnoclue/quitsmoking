
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Message</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/dashboard.css" />"
	rel="stylesheet">

<script
	src="${pageContext.request.contextPath}/resources/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img src="images/main/logo_black.png" width="280px" alt="" />
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<form action="/search" method="POST" style="margin-top: 33px;">
					<ul class="nav navbar-nav navbar-right">
						<li><input type="text" name="keyword" value=""
							placeholder="Search members" /></li>
						<li><input type="image"
							src="/resources/images/glyphicons-28-search.png" alt="search"
							style="background-color: white; height: 26px" /></li>
					</ul>
				</form>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
						
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
			

	

          <h1 class="page-header">Subject: ${message.subject }</h1>
          <h3><a href="<c:url value="/${place}" /> ">Back</a></h3>
          <div style="    background-color: #f1f1f1;
    padding: 0.01em 16px; margin-top:20px;">
            <span> <b>From: <a href="<c:url value="/profile?userId=${message.fromUserId}"/>">${uidUsernameMap.get(message.fromUserId) }</a></b></span><br>
            <p>${message.content}</p>
            <span style="color:grey">${message.getDisplayTime()}</span>
          </div>

		

          <form action="sendMessage">
            <div class="form-group">
            <div style="height:20px"></div>
              <label for="comment">Reply Message:</label>
              

              <span>Subject:</span>
              <input name="subject" type="text" class="form-control" id="subject" value="RE: ${message.subject}">
              <input type="hidden" name="toUserId" value="${message.fromUserId}">
              
              <div style="margin-top:10px;">
                <textarea name="content" class="form-control" rows="5" id="content"></textarea>
              </div>

            </div>
            <button style="float:right; margin-left:10px;" type="submit"  class="btn btn-default">Submit</button>
            <button style="float:right;" class="btn btn-default" type="reset">Clear</button>
          </form>

















        
        </div>
      </div>


    </div>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/holder.min.js" />"></script>
	<script
		src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"></script>


</body>
</html>