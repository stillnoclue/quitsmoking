<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main content</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<link href="css/main.css" type="text/css" rel="stylesheet" />
<link rel="shortcut icon" href="images/main/favicon.ico" />
    <link href="css/dhtmlx_pro_full/dhtmlx.css" rel="stylesheet" type="text/css"></link>
    <link href="css/jquery.qtip.min.css" rel="stylesheet" type="text/css"></link>
	<script src="css/dhtmlx_pro_full/dhtmlx.js"></script>
	<script src="js/jquery-1.10.1.min.js"></script>
	<script  src="js/jquery.validate.min.js"></script>
	<script  src="js/messages_en.js"></script>
	<script  src="js/jquery.form.min.js"></script>
    <script  src="js/My97DatePicker/WdatePicker.js"></script>
    <script  src="js/jquery.qtip.min.js"></script> 
<style>
body{overflow-x:hidden; background:#f2f0f5; padding:15px 0px 10px 5px;}
#searchmain{ font-size:12px;}
#search{ font-size:12px; background:#548fc9; margin:10px 10px 0 0; display:inline; width:100%; color:#FFF; float:left}
#search form span{height:40px; line-height:40px; padding:0 0px 0 10px; float:left;}
#search form input.text-word{height:24px; line-height:24px; width:180px; margin:8px 0 6px 0; padding:0 0px 0 10px; float:left; border:1px solid #FFF;}
#search form input.text-but{height:24px; line-height:24px; width:55px; background:url(images/main/list_input.jpg) no-repeat left top; border:none; cursor:pointer; font-family:"Microsoft YaHei","Tahoma","Arial",'宋体'; color:#666; float:left; margin:8px 0 0 6px; display:inline;}
#search a.add{ background:url(images/main/add.jpg) no-repeat -3px 7px #548fc9; padding:0 10px 0 26px; height:40px; line-height:40px; font-size:14px; font-weight:bold; color:#FFF; float:right}
#search a:hover.add{ text-decoration:underline; color:#d2e9ff;}
#main-tab{ border:1px solid #eaeaea; background:#FFF; font-size:12px;}
#main-tab th{ font-size:12px; background:url(images/main/list_bg.jpg) repeat-x; height:32px; line-height:32px;}
#main-tab td{ font-size:12px; line-height:40px;}
#main-tab td a{ font-size:12px; color:#548fc9;}
#main-tab td a:hover{color:#565656; text-decoration:underline;}
.bordertop{ border-top:1px solid #ebebeb}
.borderright{ border-right:1px solid #ebebeb}
.borderbottom{ border-bottom:1px solid #ebebeb}
.borderleft{ border-left:1px solid #ebebeb}
.gray{ color:#dbdbdb;}
td.fenye{ padding:10px 0 0 0; text-align:right;}
.bggray{ background:#f9f9f9}
</style>
   <script language="javascript">
		
		var dialogwins;
		
		window.onload=function(){
			dialogwins= new dhtmlXWindows();
		}
		</script>
	<script language="javascript">
		 
	     
     
          function forbidden(a) {
          dhtmlx.confirm({
					title:"Attention",
					text: "Confirm to forbidden the user?",
					ok:"Confirm",
					cancel:"Cancel",										    				
					callback: function(flag) {
						  if(flag){
					        	$("#form1").ajaxSubmit({
								url:"userstatus/1/"+a,
								type:'POST',
								dataType:'json',
								success:function(data){
									dhtmlx.alert({title:"Attention",text:data.msg,ok:"OK",callback: function() {
										if(data.success){								
											location.href=data.url;
										}
									}});					
								}
							});
							return false;
				   		}
					}
				});
			   }
			    
			    
			      function deblocking(a) {
          dhtmlx.confirm({
					title:"Attention",
					text: "Confirm to release the user's forbidden?",
					ok:"Confirm",
					cancel:"Cancel",										    				
					callback: function(flag) {
						  if(flag){
					        	$("#form1").ajaxSubmit({
								url:"userstatus/0/"+a,
								type:'POST',
								dataType:'json',
								success:function(data){
									dhtmlx.alert({title:"Attention",text:data.msg,ok:"OK",callback: function() {
										if(data.success){								
											location.href=data.url;
										}
									}});					
								}
							});
							return false;
				   		}
					}
				});
			   }
			   
			   
			   
		 
			
	</script>
	</head>
<body>
<!--main_top-->
<form id="form1" name="form1" method="post">
<table width="99%" border="0" cellspacing="0" cellpadding="0" id="searchmain">
  <tr>
    <td width="99%" align="left" valign="top">Your Position: Member List</td>
  </tr> 
  <tr>
    <td align="left" valign="top">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="main-tab">
      <tr>
        <th align="center" valign="middle" class="borderright">Username</th>
        <th align="center" valign="middle" class="borderright">First Name</th>
        <th align="center" valign="middle" class="borderright">Last Name</th>
        <th align="center" valign="middle" class="borderright">E-Mail</th>
        <th align="center" valign="middle" class="borderright">Status</th>
        <th align="center" valign="middle">Action</th>
      </tr>
     
	<c:forEach items="${smokerlist}" var="user">    
      <tr onMouseOut="this.style.backgroundColor='#ffffff'" onMouseOver="this.style.backgroundColor='#edf5ff'">
        <td align="center" valign="middle" class="borderright borderbottom">${user.username}</td>
        <td align="center" valign="middle" class="borderright borderbottom">${user.firstname}</td>
        <td align="center" valign="middle" class="borderright borderbottom">${user.lastname}</td>
        <td align="center" valign="middle" class="borderright borderbottom">${user.email}</td>
        <td align="center" valign="middle" class="borderright borderbottom">
 		 <c:if test="${user.status==0}">Normal</c:if>
        <c:if test="${user.status==1}">Forbidden</c:if> 
        
</td>
        <td align="center" valign="middle" class="borderbottom">
       <input name="btnApproval" type="button" id="btnApproval" value="Forbidden"  onclick='forbidden(${user.uid})'  /> 
        <span class="gray">&nbsp;|&nbsp;</span>
        <input name="btnDenied" type="button" id="btnDenied" value="Release"   onclick='deblocking(${user.uid})'/> 
        
        </td>
      </tr>
    </c:forEach>
    </table></td>
    </tr>
  
</table>
</form>
</body>
</html>