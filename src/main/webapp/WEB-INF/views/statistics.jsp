<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Statistics</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/dashboard.css" />"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- All time -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette(s) smoked'],
           <c:forEach var="answ" items="${answers}">
                [${answ.answerdate}+"", ${answ.cigarettes}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette Consuption',
                colors: ['#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'No of cigarette(s) smoked'
                }
              };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_cig_alltime'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette pack(s) bought'],
           <c:forEach var="answ" items="${answers}">
                [${answ.answerdate}+"", ${answ.packs}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette pack bought',
                hAxis: {
                    title: 'Date',

                  },
                  vAxis: {
                      title: 'No of cigarette pack(s) bought',
                  }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_pack_alltime'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost'],
           <c:forEach var="answ" items="${answers}">
                [${answ.answerdate}+"", ${answ.cost}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cost behind cigarettes - Last 30 days',
                colors: ['#60bd68','#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'Cost'
                }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_cost_alltime'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<!-- Last 7 days -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette(s) smoked'],
           <c:forEach var="wansw" items="${weeklyAnswers}">
                [${wansw.answerdate}+"", ${wansw.cigarettes}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette Consumption - Last 7 days',
                colors: ['#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'No of cigarette(s) smoked'
                }
              };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_cig_7days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette pack(s) bought'],
           <c:forEach var="wansw" items="${weeklyAnswers}">
                [${wansw.answerdate}+"", ${wansw.packs}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette pack bought - Last 7 days',
                hAxis: {
                  title: 'No of cigarette pack(s) bought',
                },
                vAxis: {
                  title: 'Date'
                }
              };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div_pack_7days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost'],
           <c:forEach var="wansw" items="${weeklyAnswers}">
                [${wansw.answerdate}+"", ${wansw.cost}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cost behind cigarettes - Last 7 days',
                colors: ['#60bd68','#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'Cost'
                }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_cost_7days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<!-- Last 30 days -->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette(s) smoked'],
           <c:forEach var="mansw" items="${monthlyAnswers}">
                [${mansw.answerdate}+"", ${mansw.cigarettes}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette Consumption - Last 30 days',
                colors: ['#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'No of cigarette(s) smoked'
                }
              };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_cig_30days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'No of cigarette pack(s) bought'],
           <c:forEach var="mansw" items="${monthlyAnswers}">
                [${mansw.answerdate}+"", ${mansw.packs}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette pack bought - Last 30 days',
                hAxis: {
                    title: 'No of cigarette pack(s) bought',
                  },
                  vAxis: {
                    title: 'Date'
                  }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_pack_30days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost'],
           <c:forEach var="mansw" items="${monthlyAnswers}">
                [${mansw.answerdate}+"", ${mansw.cost}], 
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cost behind cigarettes - Last 30 days',
                colors: ['#60bd68','#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Date',
                },
                vAxis: {
                  title: 'Cost'
                }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_cost_30days'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<!-- Annual data -->

<c:set var="counter"  value="${1}"/>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'No of cigarette(s) smoked'],
           <c:forEach var="aansw" items="${annualAnswers}" varStatus="theCount">
                [${counter}+"", ${aansw.cigarettes}], 
                <c:set var="counter"  value="${counter+1}"/>
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette Consumption - This year',
                colors: ['#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Month',
                },
                vAxis: {
                  title: 'No of cigarette(s) smoked'
                }
              };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_cig_annual'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<c:set var="counter"  value="${1}"/>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'No of cigarette pack(s) bought'],
           <c:forEach var="aansw" items="${annualAnswers}">
                [${counter}+"", ${aansw.packs}], 
                <c:set var="counter"  value="${counter+1}"/>
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cigarette pack bought - This year',
                hAxis: {
                    title: 'No of cigarette pack(s) bought',
                  },
                  vAxis: {
                    title: 'Month'
                  }
              };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div_pack_annual'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>

<c:set var="counter"  value="${1}"/>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Cost'],
           <c:forEach var="aansw" items="${annualAnswers}">
                [${counter}+"", ${aansw.cost}], 
                <c:set var="counter"  value="${counter+1}"/>
            </c:forEach>
        ]);
        
        var options = {
                title: 'Cost behind cigarettes - This year',
                colors: ['#60bd68','#ec8f6e', '#f6c7b6', '#e0440e', '#e6693e', '#ec8f6e', '#f3b49f'],
                hAxis: {
                  title: 'Month',
                },
                vAxis: {
                  title: 'Cost'
                }
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div_cost_annual'));

        //chart.draw(data, {width: 800, height: 550, min: 0});
        chart.draw(data, options);
      }
    </script>
<script type="text/javascript">
	$('.nav-tabs a').click(function(){
	    $(this).tab('show');
	})

/* 	// Select tab by name
	$('.nav-tabs a[href="#home"]').tab('show')

	// Select first tab
	$('.nav-tabs a:first').tab('show') 

	// Select last tab
	$('.nav-tabs a:last').tab('show') 

	// Select fourth tab (zero-based)
	$('.nav-tabs li:eq(3) a').tab('show') */
	</script>

</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img src="images/main/logo_black.png" width="280px" alt="" />
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<form action="/search" method="POST" style="margin-top: 33px;">
					<ul class="nav navbar-nav navbar-right">
						<li><input type="text" name="keyword" value=""
							placeholder="Search members" /></li>
						<li><input type="image"
							src="/resources/images/glyphicons-28-search.png" alt="search"
							style="background-color: white; height: 26px" /></li>
					</ul>
				</form>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>

						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1>Statistics</h1>
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#last7days">Last
							7 days</a></li>
					<li><a data-toggle="tab" href="#last30days">Last month</a></li>
					<li><a data-toggle="tab" href="#lastyear">This year</a></li>
					<li><a data-toggle="tab" href="#alltime">All Time</a></li>
				</ul>

				<div class="tab-content">
					<div id="last7days" class="tab-pane fade in active">
						<h3>Statistics for last 7 days</h3>
					<table>
						<tr>
							<td>
								<div id="chart_div_cig_7days"
									style="width: 600px; height: 340px;"></div>
							</td>
							<td><div id="chart_div_pack_7days"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
						<tr>
							<td><div id="chart_div_cost_7days"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
					</table>
					</div>
					<div id="last30days" class="tab-pane fade">
						<h3>Statistics for last month</h3>
					<table>
						<tr>
							<td>
								<div id="chart_div_cig_30days"
									style="width: 600px; height: 340px;"></div>
							</td>
							<td><div id="chart_div_pack_30days"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
						<tr>
							<td><div id="chart_div_cost_30days"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
					</table>

					</div>
					<div id="lastyear" class="tab-pane fade">
						<h3>Statistics for this year</h3>

					<table>
						<tr>
							<td>
								<div id="chart_div_cig_annual"
									style="width: 600px; height: 340px;"></div>
							</td>
							<td><div id="chart_div_pack_annual"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
						<tr>
							<td><div id="chart_div_cost_annual"
									style="width: 600px; height: 340px;"></div></td>
						</tr>
					</table>
					</div>
					<div id="alltime" class="tab-pane fade">
						<h3>Statistics for life time</h3>
						<table>
							<tr>
								<td>
									<div id="chart_div_cig_alltime"
										style="width: 600px; height: 340px;"></div>
								<td>
									<div id="chart_div_pack_alltime"
										style="width: 600px; height: 340px;"></div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="chart_div_cost_alltime"
										style="width: 600px; height: 340px;"></div>
								</td>
							</tr>
						</table>
					</div>
				</div>


				<%-- 				
				<h2>All data entries</h2>
				<table class="table table-striped table-bordered" width="100%"
					cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Pack(s) bought</th>
							<th>Cigarette(s) Smoked</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="ans" items="${answers}">
							<tr>
								<td>${ans.answerdate }</td>
								<td>${ans.packs }</td>
								<td>${ans.cigarettes }</td>

							</tr>
						</c:forEach>
					</tbody>
				</table> --%>

			</div>



		</div>


	</div>




	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/holder.min.js" />"></script>
	<script
		src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"></script>


</body>
</html>

