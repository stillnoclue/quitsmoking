
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>${post.topic }</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/dashboard.css" />"
	rel="stylesheet">

<script
	src="${pageContext.request.contextPath}/resources/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img src="images/main/logo_black.png" width="280px" alt="" />
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<form action="/search" method="POST" style="margin-top: 33px;">
					<ul class="nav navbar-nav navbar-right">
						<li><input type="text" name="keyword" value=""
							placeholder="Search members" /></li>
						<li><input type="image"
							src="/resources/images/glyphicons-28-search.png" alt="search"
							style="background-color: white; height: 26px" /></li>
					</ul>
				</form>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
						
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	
          <h1 class="page-header">${post.topic}</h1>
          <h3><a href="<c:url value="/list?boardId="/>${post.boardId}">Back</a></h3>
          <div style="    background-color: #f1f1f1;
    padding: 0.01em 16px; margin-top:20px;">
            <span>by: <b><a href="<c:url value="/profile?userId=${post.author}"/>">${uidUsernameMap.get(post.author) }</a></b></span><br>
            <p>${post.content}</p>
            <span style="color:grey">${post.getDisplayTime()}</span>
            <a href="<c:url value="/composeMessage?toUserId=${post.author}" /> ">send message</a>
          </div>
					<c:forEach var="tempReply" items="${replies}">
								<div style="    background-color: #f2f2f2;
						    padding: 0.01em 16px; margin-top:20px;">
						
						
						              <p>${tempReply.content}</p>
						                <span style="float:right; color:grey">${tempReply.getDisplayTime()}</span><br>
						                <span style="float:right;">by: <b><a href="<c:url value="/profile?userId=${tempReply.author}"/>">${uidUsernameMap.get(tempReply.author) }</a></b></span><br>
						                <a href="<c:url value="/post?postId=${tempReply.postId}&replyToReplyId=${tempReply.replyId}" /> ">reply</a>
						                <a href="<c:url value="/composeMessage?toUserId=${tempReply.author}" /> ">send message</a>
						                
						                
						          </div>
					</c:forEach>

		




          <form action="newReply" method="POST">
          
            <div class="form-group">
            <div style="height:20px"></div>
              <label for="comment" >Reply:</label>
              <div style="margin-top:10px;">
                 <c:choose>
                <c:when test="${not empty param.replyToReplyId}">
                <c:set var="preFilledContent" scope="session" value="Quote: ${replyToReply. author } Said: '${replyToReply.content}'"/>
                
                </c:when>
               	<c:otherwise>
               	<c:set var="preFilledContent" scope="session" value=""/>
               	</c:otherwise>
                </c:choose>
              
                <textarea name="content" class="form-control" rows="5" id="comment">${preFilledContent}</textarea>
                <div class="g-recaptcha" data-sitekey="6LdvMAoUAAAAAKMek9nKI0N3zSgQoZejI2oP_qpp"></div>
                <input type="hidden" name="postId" value="${post.postId}">
                <input type="hidden" name="replyToReplyId" value="${param.replyToReplyId}">
                
                
                
              </div>

            </div>
            
            <button style="float:right; margin-left:10px;" type="submit"  class="btn btn-default">Submit</button>
            <button style="float:right;" class="btn btn-default" type="reset">Clear</button>
          </form>






        </div>
      </div>


    </div>



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/holder.min.js" />"></script>
	<script
		src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"></script>


</body>
</html>
