<%@ include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Forum</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="<c:url value="/resources/css/ie10-viewport-bug-workaround.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/dashboard.css" />"
	rel="stylesheet">

<script
	src="${pageContext.request.contextPath}/resources/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img src="images/main/logo_black.png" width="280px" alt="" />
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<form action="/search" method="POST" style="margin-top: 33px;">
					<ul class="nav navbar-nav navbar-right">
						<li><input type="text" name="keyword" value=""
							placeholder="Search members" /></li>
						<li><input type="image"
							src="/resources/images/glyphicons-28-search.png" alt="search"
							style="background-color: white; height: 26px" /></li>
					</ul>
				</form>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<c:choose>
						<c:when test="${loginUser.role == 1 }">
							<jsp:include page="navigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 2 }">
							<jsp:include page="gpnavigation.jsp"></jsp:include>
						</c:when>
						<c:when test="${loginUser.role == 3 }">
							<jsp:include page="adminnavigation.jsp"></jsp:include>
						</c:when>
						<c:otherwise>
						
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<form action="/updateProfile" method="POST">
					<table class="table-striped" style="width: 100%; font-size: 12pt;">
						<tbody>
							<tr>
								<td>Username:</td>
								<td>${profileUser.username }</td>
							</tr>
							<tr>
								<td>Firstname:</td>
								<td><input type="text" name="firstname"
									pattern="^[a-zA-Z0-9 ]*$"
									title="Can only contain letters numbers and space( )"
									value="${profileUser.firstname}" /></td>
							</tr>
							<tr>
								<td>Lastname:</td>
								<td><input type="text" name="lastname"
									pattern="^[a-zA-Z0-9 ]*$"
									title="Can only contain letters numbers and space( )"
									value="${profileUser.lastname}" /></td>
							</tr>
							<tr>
								<td>Date Of Birth:</td>
								<td><input type="text" name="date_of_birth"
									value="${profileUser.date_of_birth}
									onclick="
									WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly /></td>
							</tr>
							<tr>
								<td>Phone number:</td>
								<td><input type="text" name="phonenum" pattern="[0-9]+"
									title="Must contain numbers only."
									value="${profileUser.phonenum}" /></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><input type="text" name="email"
									pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
									title="Please input valid email format. Example. abc@nodmain.com"
									value="${profileUser.email}" /></td>
							</tr>
							<c:if test="${profileUser.role == 2}">
								<tr>
									<td>GP License:</td>
									<td><input type="text" name="gp_license"
										value="${profileUser.gp_license}" disabled /></td>
								</tr>
							</c:if>
							<tr>
								<td>About Me:</td>
								<td><textarea rows="5" cols="22" name="aboutme">${profileUser.aboutme}</textarea></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" value="update" class="btn btn-primary" />
				</form>
				<br>
				<br>
				<form action="changePassword" method="POST">
					<table class="table-striped" style="width: 79%; font-size: 12pt;">
						<tbody>
							<tr>
								<td>Current Password:</td>
								<td><input type="password" placeholder="Old Password"
									id="oldPassword" name="oldPassword" required></td>
							</tr>
							<tr>
								<td>New password:</td>
								<td><input type="password" placeholder="New Password"
								pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
									title="Password must be at least 8 characters long. Must contain at least one uppercase letter, one lowercase letter and one number."
									id="password" required name="newPassword"></td>
							</tr>
							<tr>
								<td>Confirm New Password:</td>
								<td><input type="password" placeholder="Confirm New Password"
								pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
									title="Password must be at least 8 characters long. Must contain at least one uppercase letter, one lowercase letter and one number."
									id="confirm_password" required></td>
							</tr>
							<tr>

							</tr>
						</tbody>
					</table>
					<button type="submit" class="btn btn-primary">Change</button>
				</form>
			</div>
		</div>
	</div>
	<c:if test="${ msgTitle != null}">
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">${msgTitle}</h4>
					</div>
					<div class="modal-body">
						<p>${msgContent}</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</c:if>
	<!-- Script to confirm the new password -->
	<script type="text/javascript">
	var password = document.getElementById("password")
	  , confirm_password = document.getElementById("confirm_password");

	function validatePassword(){
	  if(password.value != confirm_password.value) {
	    confirm_password.setCustomValidity("Passwords Don't Match");
	  } else {
	    confirm_password.setCustomValidity('');
	  }
	}

	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
	</script>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/holder.min.js" />"></script>
	<script
		src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"></script>

		<!-- Showing Message on Error or Success
	==================================================  -->
		<c:if test="${ msgTitle != null}">
			<script type="text/javascript">
		    $(window).load(function(){
		        $('#myModal').modal('show');
		    });
			</script>
		</c:if>
</body>
</html>
