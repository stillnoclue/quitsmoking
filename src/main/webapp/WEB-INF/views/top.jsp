<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>top</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
</head>
<body onselectstart="return false" oncontextmenu=return(false) style="overflow-x:hidden;">
<noscript><iframe scr="*.htm"></iframe></noscript>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="header">
  <tr>
    <td rowspan="2" align="left" valign="top" id="logo"><img src="images/login/logo.png" height="72"></td>
    <td align="left" valign="bottom">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="bottom" id="header-name"></td>
        <td align="right" valign="top" id="header-right">
        	<a href="/adminlogout" target="_top" onFocus="this.blur()" class="admin-out">Log Out</a>
            <a href="/main" target="_top" onFocus="this.blur()" class="admin-home">Admin Index</a>
        	<a href="/" target="_top" onFocus="this.blur()" class="admin-index">Index</a>       	
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="bottom">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top" id="header-admin"></td>
        <td align="left" valign="bottom" id="header-menu">
        <a href="/main" target="_top" onFocus="this.blur()" id="menuon">Main</a>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>