<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>bottom</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<style>
#footer{font-size:12px;}
.footer_pad{padding:7px 9px 5px 9px;}
</style>
</head>
<body style="overflow-x:hidden; background:url(images/main/bottombg.jpg) repeat-x top left;" onselectstart="return false" oncontextmenu=return(false) >
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="footer">
  <tr>
    <td align="left" valign="middle" class="footer_pad">COPYRIGHT©2016    USYD ELEC5619 GROUP 5 </td>
    <td align="right" valign="middle" class="footer_pad"><img src="images/main/why.gif" width="12" height="12" alt="contact" align="absmiddle">&nbsp;&nbsp;Contact E-mail: tjin0016@uni.sydney.edu.au</td>
  </tr>
</table>
</body>
</html>