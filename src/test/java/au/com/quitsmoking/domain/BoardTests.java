package au.com.quitsmoking.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BoardTests {

	private Board board;
	
	protected void SetUp() throws Exception {
		board = new Board();
	}
	
	public void testSetAndGetBoardid() {
		int testBoardid = 100;
		assertNull(board.getBoardId());
		board.setBoardId(testBoardid);
		assertEquals(testBoardid, board.getBoardId());
	}
	
	public void testSetAndGetBoardname() {
		String testBoardname = "Story";
		assertNull(board.getBoardName());
		board.setBoardName(testBoardname);
		assertEquals(testBoardname, board.getBoardName());
	}
	
	public void testSetAndGetPostcount() {
		int testPostcount = 1;
		assertNull(board.getPostCount());
		board.setBoardId(testPostcount);
		assertEquals(testPostcount, board.getPostCount());
	}

	public void testSetAndGetLastPostDate() {
		String testLastPostDate = "2014-10-23 09:23:23.231";
		assertNull(board.getLastPostDate());
		board.setLastPostDate(testLastPostDate);
		assertEquals(testLastPostDate, board.getLastPostDate());
	}
	
	public void testSetAndGetLastpostid() {
		int testLastPostId = 1;
		assertNull(board.getLastPostId());
		board.setBoardId(testLastPostId);
		assertEquals(testLastPostId, board.getLastPostId());
	}
}
