package au.com.quitsmoking.domain;

import junit.framework.TestCase;

public class UserTest extends TestCase {

	User user = new User();
	
	public void testSetAndGetAboutme() {
		String testAboutme="about me";
		assertNull(user.getAboutme());
		user.setAboutme(testAboutme);
		assertEquals(testAboutme, user.getAboutme());
	}
	
	public void testSetAndGetCreatedDate() {
		String testDate="10/15/2016";
		assertNull(user.getCreated_date());
		user.setCreated_date(testDate);
		assertEquals(testDate, user.getCreated_date());
	}
	
	public void testSetAndGetDateOfBirth() {
		String testDate="15/12/2016";
		assertNull(user.getDate_of_birth());
		user.setDate_of_birth(testDate);
		assertEquals(testDate, user.getDate_of_birth());
	}
	public void testSetAndGetEmail() {
		String testEmail="nobody@nodomain.com.au";
		assertNull(user.getEmail());
		user.setEmail(testEmail);;
		assertEquals(testEmail, user.getEmail());
	}
	public void testSetAndGetFirstName() {
		String testName="firstname";
		assertNull(user.getFirstname());
		user.setFirstname(testName);;
		assertEquals(testName, user.getFirstname());
	}
	public void testSetAndGetLastName() {
		String testName="lastname";
		assertNull(user.getLastname());
		user.setLastname(testName);
		assertEquals(testName, user.getLastname());
	}
	public void testSetAndGetPassword() {
		String testPassword="password";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		assertEquals(testPassword, user.getPassword());
	}
	public void testSetAndGetPhoneNum() {
		String testNumber="lastname";
		assertNull(user.getPhonenum());
		user.setPhonenum(testNumber);
		assertEquals(testNumber, user.getPhonenum());
	}
	public void testSetAndGetGPLicense() {
		String testLicense="123123213";
		assertNull(user.getGp_license());
		user.setGp_license(testLicense);
		assertEquals(testLicense, user.getGp_license());
	}
	public void testSetAndGetGPID() {
		long gpid = 1;
		assertEquals(0, user.getGpid());
		user.setGpid(gpid);;
		assertEquals(gpid, user.getGpid());
	}
	public void testSetAndGetReceiveEmail() {
		int testReceiveEmail = 1;
		assertEquals(0, user.getReceive_email());
		user.setReceive_email(testReceiveEmail);
		assertEquals(testReceiveEmail, user.getReceive_email());
	}
	public void testSetAndGetRole() {
		int testRole = 1;
		assertEquals(0, user.getRole());
		user.setRole(testRole);
		assertEquals(testRole, user.getRole());
	}
	public void testSetAndGetStatus() {
		int testStatus = 1;
		assertEquals(0, user.getStatus());
		user.setStatus(testStatus);
		assertEquals(testStatus, user.getStatus());
	}
	public void testSetAndGetUid() {
		int testUid = 1;
		assertEquals(0, user.getUid());
		user.setUid(testUid);
		assertEquals(testUid, user.getUid());
	}
	public void testSetAndGetUsername() {
		String testUsername = "username";
		assertNull(user.getUsername());
		user.setUsername(testUsername);
		assertEquals(testUsername, user.getUsername());
	}
}
