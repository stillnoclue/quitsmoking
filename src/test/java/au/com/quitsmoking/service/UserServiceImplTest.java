package au.com.quitsmoking.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import au.com.quitsmoking.domain.User;
import junit.framework.TestCase;

public class UserServiceImplTest extends TestCase {

	private UserService userService;
	private List<User> users;
	
	private static long UID1 = 100;
	private static long UID2 = 101;
	
	protected void setUp() throws Exception {
		userService = new UserServiceImpl();
		
		//set up a list of users
		//user 100
		User user = new User();
		user.setUid(100);
		user.setUsername("usertest");
		user.setFirstname("user");
		user.setLastname("test");
		user.setPassword("Password1");
		user.setCreated_date("20150909");
		user.setRole(1);
		user.setStatus(0);
		user.setDate_of_birth("19900909");
		user.setEmail("usertest@gmail.com");
		user.setPhonenum("0430999888");
		user.setReceive_email(0);
		users.add(user);
		userService.addUser(user);
		
		//user 101
		user = new User();
		user.setUid(101);
		user.setUsername("usertest2");
		user.setFirstname("user2");
		user.setLastname("test2");
		user.setPassword("Password2");
		user.setCreated_date("20150902");
		user.setRole(1);
		user.setStatus(0);
		user.setDate_of_birth("19900902");
		user.setEmail("usertest2@gmail.com");
		user.setPhonenum("0430999882");
		user.setReceive_email(0);
		users.add(user);
		userService.addUser(user);
	}

	public void testGetUsersWithNoUser() {
		userService = new UserServiceImpl();
		assertNull(userService.getAllUserList());
	}
	
	public void testGetUserByUID() {
		User r = userService.getUserByUID(UID1);
		User ul = new User();
		for (User uu : users) {
			if (uu.getUid()==UID1) {
				ul.setUid(uu.getUid());
				ul.setUsername(uu.getUsername());
				ul.setFirstname(uu.getFirstname());
				ul.setLastname(uu.getLastname());
				ul.setPassword(uu.getPassword());
				ul.setCreated_date(uu.getCreated_date());
				ul.setRole(uu.getRole());
				ul.setStatus(uu.getStatus());
				ul.setDate_of_birth(uu.getDate_of_birth());
				ul.setEmail(uu.getEmail());
				ul.setPhonenum(uu.getPhonenum());
				ul.setReceive_email(uu.getReceive_email());
			}
		}
		assertEquals(r, ul);
	}
}
