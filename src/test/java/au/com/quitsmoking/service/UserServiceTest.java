package au.com.quitsmoking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import au.com.quitsmoking.domain.User;
import junit.framework.TestCase;

public class UserServiceTest  extends TestCase{
	@Autowired
	private UserService userservice;
	
	public void testGetUserList(){
		List<User> userlist = this.userservice.getUserList();
		assertNotNull(userlist);
	}
	
	public void testAddUser(){
		User user = new User();
		user.setUsername("test");
		user.setFirstname("test");
		user.setLastname("test");
		user.setPassword("test");
		user.setCreated_date("20161024");
		user.setRole(1);
		user.setStatus(0);
		user.setDate_of_birth("19891212");
		user.setEmail("test@nodomain.com");
		user.setPhonenum("1212121");
		user.setReceive_email(1);
		this.userservice.addUser(user);
		User dbUser = this.userservice.getUserByUsername("test");
		assertNotNull(dbUser);
	}
}
