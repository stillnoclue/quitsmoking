# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository main branch is "new".
do not push into new unless you have working solution.
Every other experimental stuffs need to go under your own branch.

* Version
1.

### How do I get set up? ###

* Summary of set up
Maven
tomcat 8
eclipse IDE

* Configuration
clone the repository
import into eclipse
choose import from git >> existing local repository >> choose cloned repo
right click on project folder >> properties >> java build path >> libraries >> link missing library
* Dependencies
Spring
aop
maven
hibernate
* Database configuration
table scripts to create database can be downloaded from google drive
username root
password root
dbname group5
* How to run tests
right click on project folder >> run as >> run on server
open your broswer and type localhost:8080.
Do not rely on eclipse browser as it is not good enough.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
Test cases need to be written for domain, controller and services
* Code review
If you are not sure of why your code is not working or some issues you cannot solve there, please push it into review branch. 
* Other guidelines
Check wiki for details.

### Who do I talk to? ###

* Repo owner or admin
Everyone in the group.
* Other community or team contact
----------------------------------------------------------------------------------
### How to Solve the Deployment Dependency Problem? ###
* manually update the pom.xml file
it configs the md5/json/mail API
* Solve by Setting Properties
add the external java-json.jar through the Property Setting -> Deployment Assembly -> Add external jar library

### Accounts Pre-defined ###
Common Password: Password1

**Admin:**

admin

**Member:**

normal:

    jackquit

    extraclean

    test123

    baoming

    taolin

    robinz
    
forbidden:

    anothertj

    trte9090
    
**GP:**

saveworld - approved

gpkevin - in process

fakegp - not approved

----------------------------------------------------------------------------------
### Responsibilities ###
* Ahnaf Tabrez Alam: Visualization, GP Dashboard
* Taolin Jin: Login, Register, Admin System
* Krishna Kant: Questionnaires, Achievement
* Nyan Thit Lwin: Settings, Profile (Show & Edit), Home
* Denghui Wang: Forum (Question Show & Post New Question)
